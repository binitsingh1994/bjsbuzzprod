<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
 
//unused pagebar skeleton when ads are disabled #628 
if(ossn_is_hook('newsfeed', "sidebar:right")) {
	$newsfeed_right = ossn_call_hook('newsfeed', "sidebar:right", NULL, array());
	$sidebar = implode('', $newsfeed_right);
	$isempty = trim($sidebar);
}  
?>
<div class="container">
	<div class="row">
       	<?php echo ossn_plugin_view('theme/page/elements/system_messages'); ?>    
		<div class="ossn-layout-newsfeed">
        	<?php 
				if(ossn_isLoggedin()){
			?>
        	<div class="col-md-2 sidebar-cont">
				<?php 
					echo ossn_plugin_view('theme/page/elements/sidebar');
				?>
			</div>
			<div class="col-md-3 col-sm-push-6">
            	<?php if(!empty($isempty)){ ?>
				<div class="newsfeed-right">
					<?php
						echo $sidebar;
						?>                            
				</div>
                <?php } ?>
			</div>
			<div class="col-md-6 col-sm-pull-3">
				<div class="newsfeed-middle">
					<?php echo $params['content']; ?>
				</div>
			</div>
			
			<?php 
				}
				if(!ossn_isLoggedin()){
			?>
			<div class="col-md-3 sidebar-cont">
				<?php
					$contents = ossn_view_form('login2', array(
            			'id' => 'ossn-login',
           				'action' => ossn_site_url('action/user/login'),
        			));
					echo ossn_plugin_view('widget/view', array(
						'title' => ossn_print('site:login'),
						'contents' => $contents,
					));	
				?>
				<a href="<?php echo ossn_site_url(); ?>" class="btn btn-primary col-md-12">Register</a>
			</div>
			<div class="col-md-3 col-sm-push-5">
            	<?php if(!empty($isempty)){ ?>
				<div class="newsfeed-right">
					<?php
						echo $sidebar;
						?>                            
				</div>
                <?php } ?>
			</div>
			<div class="col-md-5 col-sm-pull-3">
				<div class="newsfeed-middle">
					<?php echo $params['content']; ?>
				</div>
			</div>
			
			<?php
				}
			?>           
			
		</div>
	</div>
	<?php echo ossn_plugin_view('theme/page/elements/footer');?>
</div>