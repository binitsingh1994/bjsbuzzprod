<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$menus = $params['menu'];
?>
<div class="sidebar-menu-nav">
          <div class="sidebar-menu">
                 <ul id="menu-content" class="menu-content collapse out">
<?php                        
foreach ($menus as $name => $menu) {
	$section = 'menu-section-'.OssnTranslit::urlize($name).' ';
	$items = 'menu-section-items-'.OssnTranslit::urlize($name).' ';
	$item = 'menu-section-item-'.OssnTranslit::urlize($menu['text']).' ';
	
	$expend = '';
	$icon = "fa-angle-right";
	if($name == 'links'){
		$expend = 'in';
		$icon = "fa-newspaper-o";
	}
	if($name  == 'groups'){
		$icon = "fa-users";
	}
	$hash = md5($name);
	
    ?>
    
    <ul class="sub-menu collapse <?php echo $expend;?>" id="<?php echo $hash;?>" class="<?php echo $items;?>"> 
    <?php
	if(is_array($menu)){
	    foreach ($menu as $data) {
	    	if (OssnTranslit::urlize($data['name']) != 'photos') {
	    		if (OssnTranslit::urlize($data['name']) != 'invite-friends') {
			    	if (ossn_isAdminLoggedin ()) {
							$class = 'menu-section-item-'.OssnTranslit::urlize($data['name']);
							$data['class'] = 'menu-section-item-a-'.OssnTranslit::urlize($data['name']);
							unset($data['name']);
							unset($data['icon']);
							unset($data['section']);
							unset($data['parent']);
						
							$link = ossn_plugin_view('output/url', $data);		
							echo "<li class='{$class}'>{$link}</li>";
							unset($class);
					} elseif (!ossn_isAdminLoggedin ()) {
						if ($data['name'] !== '	') {
							$class = 'menu-section-item-'.OssnTranslit::urlize($data['name']);
							$data['class'] = 'menu-section-item-a-'.OssnTranslit::urlize($data['name']);
							unset($data['name']);
							unset($data['icon']);
							unset($data['section']);
							unset($data['parent']);
						
							$link = ossn_plugin_view('output/url', $data);		
							echo "<li class='{$class}'>{$link}</li>";
							unset($class);
						}
					}
				}
			}
    	}
	}
	echo "</ul>";
}
?>
<li style="padding-left: 10px;" class="extra-menu-startup"><i class="fa fa-list" aria-hidden="true"></i>
  <a target="new" href="https://www.bjspi.com/#/placement/118/">Startup List</a>
</li>
<li style="padding-left: 10px;" class="extra-menu-consultancy"><i class="fa fa-users" aria-hidden="true"></i>
<a target="new" class="menu-section-item-a-consultancy" href="<?php echo ossn_site_url(); ?>post/view/38">Consultancy List</a>
</li>
<li style="padding-left: 10px;" target="new" class="extra-menu-faq"><i class="fa fa-file-text-o" aria-hidden="true"></i>
  <a href="<?php echo ossn_site_url(); ?>post/view/34">FAQ</a>
</li>

	<?php if(ossn_isAdminLoggedin ()): ?>
			<li style="padding-left: 10px; background: #e74c3c;" class="">
				<i class="fa fa-check" aria-hidden="true"></i>
			  		<a href="<?php echo ossn_site_url(); ?>post_verification" target="_blank" style="color: #fff">Post Verification</a>
			</li>
	<?php endif; ?>


         </ul>
    </div>
</div>