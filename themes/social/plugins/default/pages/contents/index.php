<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2016 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
 	$wall       = new OssnWall;
	$posts = $wall->searchObject(array(
				'type' => 'user',
				'distinct' => true,
				'subtype' => 'wall',
				'limit' => 3,
				'order_by' => 'o.update_time DESC',

	));
	if(!ossn_isLoggedin()){
		echo "<style>.comments-likes {min-height:0px;}</style>"	;	
	}
?>
	   <div class="col-sm-3">
	   <div class="ossn-widget-home">
    	<?php 
			$contents = ossn_view_form('login2', array(
            		'id' => 'ossn-login',
           			'action' => ossn_site_url('action/user/login'),
        	));
			echo ossn_plugin_view('widget/view', array(
						'title' => ossn_print('site:login'),
						'contents' => $contents,
			));			
			// $contents = ossn_plugin_view('social_theme/newusers');
			// echo ossn_plugin_view('widget/view', array(
			// 			'class' => 'new-users-widget',
			// 			'title' => ossn_print('users'),
			// 			'contents' => $contents,
			// ));	
			// $contents = ossn_plugin_view('social_theme/groups');
			// echo ossn_plugin_view('widget/view', array(
			// 			'class' => 'new-groups-wiget',
			// 			'title' => ossn_print('groups'),
			// 			'contents' => $contents,
			// ));				
			?>	  
			</div> 
		<div style="color: #fff">

			<a href="https://msg91.com/startups/?utm_source=startup-banner" target="_blank" ><img src="https://msg91.com/images/startups/msg91Badge.png" width=“120" height=“90” title="MSG91 - SMS for Startups" alt="Bulk SMS - MSG91"></a>
		</div>	    			
       </div> 
       <div class="col-md-4 col-sm-push-5">
       <div class="ossn-widget-home">
    	<?php 
			$contents = ossn_view_form('signup', array(
        					'id' => 'ossn-home-signup',
        				'action' => ossn_site_url('action/user/register')
	   	 	));
			$heading = "<p>".ossn_print('its:free')."</p>";
			echo ossn_plugin_view('widget/view', array(
						'title' => ossn_print('create:account'),
						'contents' => $heading.$contents,
			));
			
			?>	
			</div>       			
       </div>      
		<div class="col-md-5 col-sm-pull-4 home-left-contents" style="margin-top: 62px">
			<?php
				if($posts) {
					foreach($posts as $post) {
						$item = ossn_wallpost_to_item($post);
						echo ossn_wall_view_template($item);
					}
		
				}
				echo ossn_view_pagination($count);
			?>
 	   </div>   
        
