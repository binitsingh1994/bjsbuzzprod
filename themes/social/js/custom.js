$(document).ready(function() {
    //registration validation
        $("#ossn-home-signup,.ossn-edit-form").validate({
            rules: {
                firstname: {
                    required: true,
                    lettersonly: true
                },
                lastname: {
                    required: true,
                    lettersonly: true
                },
                email: {
                    required: true,
                    email: true
                },
                email_re: {
                    required: true,
                    email: true
                },
                username: {
                    required: true,
                },
                password: {
                    required: true,
                    minlength: 6
                },   
                birthdate:{
                    required: true,
                    customDate: true
                } ,        
                gender: {
                    required: true,
                }
            },
            messages: {
                firstname: {
                    required: "Please enter your firstname",
                    lettersonly: "firstname should be alphabet only", 
                },
                lastname: {
                    required: "Please enter your lastname",
                    lettersonly: "lastname should be alphabet only"
                },
                username: {
                    required: "Please enter a username"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 6 characters long"
                },
                email: "Please enter a valid email address",
                email_re:"Please enter a valid email address",
                gender: "Please select gender",
                birthdate:{
                    required: "Please select date",
                    customDate: "date format is invalid"
                }
            }
        });
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
        }, "Letters only please"); 
        jQuery.validator.addMethod(
    "customDate",
    function(value, element) {
        // put your own logic here, this is just a (crappy) example
        return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    },
    "Please enter a date in the format dd/mm/yyyy."
);

    //ends
    //post resize
    var inputField = jQuery(".text_post");
    var oldValue = inputField.text();
    inputField.bind("drop", function() {
        $('.ossn-wall-post').attr('disabled', false);
        $('input.btn.btn-primary.ossn-wall-post').addClass('ac');
    });
    var textarea = document.querySelector('.ossn-wall-container textarea');
    if(textarea != null){
        textarea.addEventListener('keydown', autosize);             
    }
    function autosize(){
      var el = this;
    setTimeout(function(){    
    el.style.cssText = 'height:auto; padding:0';
    el.style.cssText = 'height:' + el.scrollHeight + 'px';
      },0);
    }
    var p_auto = document.querySelector('.ossn-wall-post');
    if(p_auto != null){
    p_auto.addEventListener('click',resize);
    }   
    function resize(){      
        $('.more').readmore({speed: 500});  
        document.querySelector('.ossn-wall-container textarea').style.height="auto";
    }
    $('.ossn-wall-post').attr('disabled', true);
    $('.ossn-wall-container textarea').keyup(function(){
        var a= $.trim($(this).val()); 
        if(a == ''){    
        $('.ossn-wall-cancel').hide();        
        $('.ossn-wall-post').attr('disabled', true);
        $('.ossn-wall-post').removeClass("ac");
    }else{
        $('.ossn-wall-post').attr('disabled', false);
        $('.ossn-wall-post').addClass("ac");
        $('.ossn-wall-cancel').show();
    }
    
    });
    $('input[name="ossn_photo"]').change(
            function(){
                if ($(this).val()) {
                    $('.ossn-wall-post').attr('disabled', false);
                    $('.ossn-wall-post').addClass("ac");
                } 
            }
            );
    $('.ossn-wall-container textarea').on('blur', function(){
        var a= $.trim($(this).val());            
        if(a == ''){            
            $(this).removeClass('input-desc-hover');
            
        }else{
            $('.ossn-wall-cancel').show();
        }
    }).on('focus', function(){
        $(this).addClass('input-desc-hover');
    });
    //ends
     //read more

    var knowmore = (function() {
    var executed = false;
    return function () {
        if (!executed) {
            executed = true;
            // do something
            $('.more').readmore({speed: 500});
            
        }
    };
    })();
    
    $(document).on( 'shown.bs.tab', 'a[data-toggle=\'tab\']', function (e) {

         // if($('#walkin').hasClass('active')){
         //        wlakinMaxcnt = $("#walkin #cnt").val();  
         //        //Id = 2; 

         //    }else{
         //        maxcnt = $("#home #cnt").val();  
         //       // Id =2;
         //    }   

          $('.more').readmore({speed: 500});          
        })


    //image popup
         function imgPopup(){
        //image modal
        
        var modal = document.getElementById('myImgModal');
        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        $(".post-contents.more img,.comment-text img").each(function(){
            jQuery(this).click(function(){  
                     modal.style.display = "block";
                     modalImg.src = $(this).attr("src");  
            })
        });
          if($('#myImgModal .close').length >0 ){
        // Get the <span> element that closes the modal
        var span = document.querySelector("#myImgModal .close");
        // When the user clicks on <span> (x), close the modal
        span.onclick = function() { 
            modal.style.display = "none";
        }
            //your code here 
        }  

    }
    //ends

    knowmore();
    imgPopup();

	//lazy loading
	var Id=2;
    var walkinId =2;
    var searchId = 2;
    var searchMaxcnt = $('.ossn-search-page #cnt').val();
    var maxcnt = $("#home #cnt").val();
    var walkinMaxcnt = $("#walkin #cnt").val();
    
    // var walkinMaxcnt = $("#home #cnt").val();
    var load = '<div class="loader"><div class="sk-spinner sk-spinner-pulse"></div></div>';
    function load_spin(callback){
        
                                       
                    callback();
    }
	 function last_msg_funtion(callback){           
        no_data = true;
        if(flag && no_data){
            flag = false;       
            var urls;
           // var urls = '?offset='+Id;
            if($('#walkin').hasClass('active')){
                urls = 'walkinpost?offset='+walkinId;
            }  
            if($('#home').hasClass('active')){
                urls= '?offset='+Id;  
            }  
            if($('.newsfeed-middle > div').hasClass('ossn-search-page')){
              urls= 'search?offset='+searchId;    
            }  
            
            $.ajax({
               type:'POST',
                url: urls,   
                async:false,                           
                success: function( html) {
                    flag = true;
                    
                     if($('#walkin').hasClass('active')){
                          var result = jQuery(html).find('.newsfeed-middle').children('.ossn-wall-item'); 
                           $('.loader').remove();
                            $('.tab-pane.active').append(result);
                            if(walkinId < walkinMaxcnt){  
                                $(load).insertAfter('div.newsfeed-middle');
                            }
                    }

                    if($('#home').hasClass('active')){
                        var result = jQuery(html).find('.tab-pane.active').children('.ossn-wall-item');
                        $('.loader').remove();
                        $('.tab-pane.active').append(result);
                        if(Id < maxcnt){  
                            $(load).insertAfter('div.newsfeed-middle');
                        } 
                    }
                    if($('.newsfeed-middle > div').hasClass('ossn-search-page')){
                        
                         var result = jQuery(html).find('.ossn-search-page .search-data').children('.ossn-users-list-item');
                        $('.loader').remove();
                        $('.ossn-search-page .search-data').append(result);
                        if(searchId < searchMaxcnt){  
                            $(load).insertAfter('div.newsfeed-middle');
                        } 
                    }                      
                    $('.more').readmore({speed: 500});                                          
                },
                error: function( data ){
                    flag = true;
                    $('.loader').hide();
                    no_data = false;
                }
            });
        }
        callback();
    }
	flag = true;
    $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() > $(document).height() - 85) {
            if($('#walkin').hasClass('active')){
                if(walkinId <= walkinMaxcnt ){
                load_spin(function(){
                 last_msg_funtion(function () {  
                        imgPopup();
                 });
                });                 
                 walkinId++;
             }else{
                $('.loader').hide();
             }            
            }
            if($('#home').hasClass('active')){
            if(Id <= maxcnt ){
                load_spin(function(){
                last_msg_funtion(function () {  
                        imgPopup();
                 });
                }); 
                 Id++;
             }else{
                $('.loader').hide();
             }                  
        }   
        if($('.newsfeed-middle > div').hasClass('ossn-search-page')){
            if(searchId < searchMaxcnt ){
             load_spin(function(){
                last_msg_funtion(function () {  
                        imgPopup();
                 });
                }); 
                 searchId++;
             }else{
                $('.loader').hide();
             }   
        } 


        //}
    }
    }); 
	//ends
    //notifiactions close outside click
    $(document).mouseup(function (e)
        {
           var menu_li = $('.ossn-notifications-friends,.ossn-notifications-messages,.ossn-notifications-notification')
            var container = $(".ossn-notifications-box,.menu-links,.ossn-topbar-dropdown-menu-content,.ossn-notifications-box");
            if ((!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            || (!menu_li.is(e.target) && menu_li.has(e.target).length === 0 )
            )
            {
                $('#ossn-notif-notification,#ossn-notif-messages,#ossn-notif-friends').removeClass('open');
                container.hide();
            }
});
    //ends
    //Anonymously Post
    $('.top').tooltip();
    //end
});    