<script type="text/javascript">
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
});
</script>

<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$user = $params['user'];
?>
<div>
	<label> <?php echo ossn_print('first:name'); ?> </label>
	<input type='text' name="firstname" required value="<?php if ($user->first_name == 'firstname') { } else { echo $user->first_name; } ?>" placeholder = "enter firstname" />
</div>
<div>
	<label> <?php echo ossn_print('last:name'); ?> </label>
	<input type='text' name="lastname" required value="<?php if ($user->last_name == 'lastname') { } else { echo $user->last_name; } ?>" placeholder = "enter lastname"/>
</div>
<div>
	<label> <?php echo ossn_print('username'); ?>  </label>
	<input type='text' name="username" required value="<?php echo $user->username; ?>" style="background:#E8E9EA;" readonly="readonly"/>
</div>
<?php if($user->email): ?>
<div>
	<label> <?php echo ossn_print('email'); ?> </label>
	<input type='text' name="email" style="background:#E8E9EA;" readonly="readonly" required value="<?php echo $user->email; ?>"/>
</div>
<?php else: ?>
<div>
	<label> <?php echo ossn_print('email'); ?> </label>
	<input type='text' name="email" required value="<?php echo $user->email; ?>"/>
</div>
<?php endif; ?>

<div>
	<label> <?php echo ossn_print('password'); ?>  </label>
	<input type='password' required name="password" value="" placeholder="enter password" />
</div> 
<?php
$fields = ossn_prepare_user_fields($user);
if($fields){
			$vars	= array();
			$vars['items'] = $fields;
			$vars['label'] = true;
			echo ossn_plugin_view('user/fields/item', $vars);
}
?>
<div>

<?php
	//profile edit form shows wrong default language #546
	$userlanguage = ossn_site_settings('language');
	 ossn_plugin_view('input/dropdown', array(
				'name' => 'language',
				'value' => $userlanguage,
				'options' => ossn_get_installed_translations(false),
				'required' => true
	));
?>
</div>
<input type="hidden" value="<?php echo $user->username; ?>" name="username"/>

<?php $passout_year_list = array('Before 2014', '2015', '2016', '2017', '2018', 'after_2018'); ?>

<div>
	<label>Passout Year</label>
	<select name="passout_year" >
		<?php foreach($passout_year_list as $passout_year): ?>
		<?php if($passout_year == $user->passout_year):?>
		     <option value="<?= $passout_year?>" selected="selected" ><?= $passout_year; ?></option>	
	    <?php else:?>
	         <option value="<?= $passout_year ?>" ><?= $passout_year; ?></option>	
	    <?php endif;?>
	<?php endforeach; ?>	
	</select>
</div> 

<?php $qualification_list = array('BE/ Btech', 'MBA', 'BBA', 'M.TECH', 'ITI', 'DIPLOMA', 'B.Sc', 'MCA', 'BCA', 'BBM', 'CA', 'MA', 'Pharma'); ?>

<div>
	<label> Qualification</label>
	<select name="qualification" >
		<?php foreach($qualification_list as $qualification): ?>
		<?php if($qualification == $user->qualification):?>
		     <option value="<?= $qualification?>" selected="selected" ><?= $qualification; ?></option>	
	    <?php else:?>
	         <option value="<?= $qualification ?>" ><?= $qualification; ?></option>	
	    <?php endif;?>
	<?php endforeach; ?>
	</select>
</div> 

<?php $cities_list = array('Bangalore', 'Mumbai', 'Kolkata', 'Chennai', 'Pune', 'Hyderabad', 'Gurgaon', 'Kerala', 'Delhi/NCR'); ?>
<div>
	<label> Current Location </label>
	<select name="current_location"  required="required">
	<?php foreach($cities_list as $city): ?>
		<?php if($city == $user->current_location):?>
		     <option value="<?= $city?>" selected="selected" ><?= $city; ?></option>	
	    <?php else:?>
	         <option value="<?= $city ?>" ><?= $city; ?></option>	
	    <?php endif;?>
	<?php endforeach; ?>
	</select>
</div>

<?php $search_type_list = array('fresher','experienced'); ?>

<div>
	<div class="radio-block">
		<label>Searching As</label>
	</div>
		<?php foreach($search_type_list as $search_type): ?>
			<?php if($search_type == $user->search_type):?>
				<input class="ossn-radio-input" type="radio" name="search_type" value="<?= $search_type; ?>" checked="checked">
				<span><?php echo $search_type; ?></span>
			<?php else: ?>
				<input class="ossn-radio-input" type="radio" required name="search_type" value="<?= $search_type; ?>" >
				<span><?php echo $search_type; ?></span>
			<?php endif; ?>
		<?php endforeach; ?>
</div> 

<?php $domain_list = array('Manual Testing', 'Automation Testing', 'Java', 'Dot Net', 'C/C++', 'Linux/Unix', 'Database/SQL', 'IOS/Android Developer', 'Content Writer','BigData/Hadoop','Web Developer','Project Manager','MSAP/ERP jobs','Embedded System','Scripting','Networking','Tech Support','L1/L2 Customer Support','System Admin/Linux','Data Entry /BPO','Data/Business Analyst','MBA Finance','MBA IT','MBA Marketing','Core ECE EE EEE','Core Mechanical/Aeronautical/Civil','MCA','M.tech','Mainframe','UI/UX designer','BBA/B.com/BA','Accountant','Medical/Pharma','DevOps','PHP Developer','Auto-Cad','MBA Generalist','It Recruiter'); ?> 

<div>
	<label> Domain</label>
	<select name="domain" >
		<?php foreach($domain_list as $domain): ?>
		<?php if($domain == $user->domain):?>
		     <option value="<?= $domain?>" selected="selected" ><?= $domain; ?></option>	
	    <?php else:?>
	         <option value="<?= $domain ?>" ><?= $domain; ?></option>	
	    <?php endif;?>
	<?php endforeach; ?>
	</select>
</div>

<div>
	<label> Company (optional)  </label>
	<input type='text' name="company" value="<?php echo $user->company; ?>" placeholder="enter company name" />
</div> 

<div>
	<label> About You (optional) </label>
	<textarea name="about_you"> <?php echo $user->about_you; ?> </textarea>
</div> 

<input type="submit" class="btn btn-primary" value="<?php echo ossn_print('save'); ?>"/>

