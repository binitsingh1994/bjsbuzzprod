<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$user = $params['user'];
$cover = new OssnProfile;

$coverp = $cover->coverParameters($user->guid);
$cover = $cover->getCoverURL($user);

if(!empty($coverp[0])){
	$cover_top = "top:{$coverp[0]};";
}
if(!empty($coverp[1])){
	$cover_left = "left:{$coverp[1]};";
}
if (ossn_isLoggedIn()) {
    $class = 'ossn-profile';
} else {
    $class = 'ossn-profile ossn-profile-tn';
}
?>
<style type="text/css">
.info-wrap {
  padding: 20px;
  font-size: 1.45rem;
  color: #3a3a3a;
  margin-top: .75rem;
  color: #7f8c8d;
}
</style>
<div class="ossn-profile container">
	<div class="row">
    	<div class="col-md-11">
			<div class="<?php echo $class; ?>">
				<div class="top-container card">
					<div id="container" class="profile-cover">
						<?php if (ossn_loggedin_user()->guid == $user->guid) { ?>
						<form id="upload-cover" style="display:none;" method="post" enctype="multipart/form-data">
							<input type="file" name="coverphoto" class="coverfile" onchange="Ossn.Clk('#upload-cover .upload');" />
							<?php echo ossn_plugin_view( 'input/security_token'); ?>
							<input type="submit" class="upload" />
						</form>
						<?php } ?>
						<img id="draggable" class="profile-cover-img" src="<?php echo $cover; ?>" style='<?php echo $cover_top; ?><?php echo $cover_left; ?>' />

					</div>

					<div class="profile-photo">
						<?php if (ossn_loggedin_user()->guid == $user->guid) { ?>
						<div class="upload-photo" style="display:none;cursor:pointer;">
							<span onclick="Ossn.Clk('.pfile');"><?php echo ossn_print('change:photo'); ?></span>

							<form id="upload-photo" style="display:none;" method="post" enctype="multipart/form-data">
								<input type="file" name="userphoto" class="pfile" onchange="Ossn.Clk('#upload-photo .upload');" />
								<?php echo ossn_plugin_view( 'input/security_token'); ?>
								<input type="submit" class="upload" />
							</form>
						</div>
						<?php } 
						$viewer= '' ; 
						if (ossn_isLoggedIn() && get_profile_photo_guid($user->guid)) { 
								$viewer = 'onclick="Ossn.Viewer(\'photos/viewer?user=' . $user->username . '\');"';
						}
						?>
						<img src="<?php echo $user->iconURL()->larger; ?>" height="170" width="170" <?php echo $viewer; ?> />
					</div>
					<div class="user-fullname"><?php echo $user->fullname; ?></div>

                    <?php echo ossn_plugin_view('profile/role', array('user' => $user)); ?>
                  	<div class="info-wrap">
					      <h4>Details:</h4>
					      <p>I am <strong><?php echo $user->search_type; ?></strong> and currently living in <strong><?php echo $user->current_location; ?></strong>, Looking job in <strong><?php echo $user->domain; ?></strong> . I have completed <strong><?php echo $user->qualification; ?></strong> in year <strong><?php echo $user->passout_year; ?></strong>.</p>
					  </div>
					  
					<div id='profile-hr-menu' class="profile-hr-menu">
						<?php echo ossn_view_menu( 'user_timeline'); ?>
					</div>

					<div id="profile-menu" class="profile-menu">
						<?php if (ossn_isLoggedIn()) { if (ossn_loggedin_user()->guid == $user->guid) { ?>
						<a href="<?php echo $user->profileURL('/edit'); ?>" class='btn-action'>
							<?php echo ossn_print( 'update:info'); ?>
						</a>
						<?php } ?>
						<?php if (ossn_loggedin_user()->guid !== $user->guid) { if (!ossn_user_is_friend(ossn_loggedin_user()->guid, $user->guid)) { if (ossn_user()->requestExists(ossn_loggedin_user()->guid, $user->guid)) { ?>
						<a href="<?php echo ossn_site_url("action/friend/remove?cancel=true&user={$user->guid}", true); ?>" class='btn-action'>
                                <?php echo ossn_print('cancel:request'); ?>
                            </a>
						<?php } else { ?>
						<a href="<?php echo ossn_site_url("action/friend/add?user={$user->guid}", true); ?>" class='btn-action'>
                                <?php echo ossn_print('add:friend'); ?>
                         </a>
						<?php } } else { ?>
						<a href="<?php echo ossn_site_url("action/friend/remove?user={$user->guid}", true); ?>"  class='btn-action'>
                            <?php echo ossn_print('remove:friend'); ?>
                        </a>
						<?php } ?>
					  	<a href="<?php echo ossn_site_url("messages/message/{$user->username}"); ?>" id="profile-message" data-guid='<?php echo $user->guid; ?>' class='btn-action'>
                        <?php echo ossn_print('message'); ?></a>
						<div class="ossn-profile-extra-menu dropdown">
							<?php echo ossn_view_menu( 'profile_extramenu', 'profile/menus/extra'); ?>
						</div>
						<?php } }?>
					</div>
					<div id="cover-menu" class="profile-menu">
						<a href="javascript:void(0);" onclick="Ossn.repositionCOVER();" class='btn-action'>
							<?php echo ossn_print('save:position'); ?>
						</a>
					</div>
				</div>

			</div>   
          </div>   
    </div>
	<div class="row ossn-profile-bottom">

	 <?php if (isset($params['subpage']) && !empty($params[ 'subpage']) && ossn_is_profile_subapge($params['subpage'])) { 
 				if (ossn_is_hook( 'profile', 'subpage')) { 
							echo ossn_call_hook('profile', 'subpage', $params);
				}
			} else { ?>   
            <div class="col-md-7">
					<div class="ossn-profile-wall">
						<?php 
						if(com_is_active('OssnWall')) { 
								$params['user'] =  $user; 
								echo ossn_plugin_view('wall/user/wall', $params); 
						} ?>
					</div>
         </div>      
		<div class="col-md-4">
		
			<style>
.widget-heading{
text-align: center;
}
.jobs-right{
margin-top: 15px;
margin-left: -45px;
}
.btn-block{
width: 250px;
}

.btn-outlined {
    border-radius: 0;
    -webkit-transition: all 0.3s;
       -moz-transition: all 0.3s;
            transition: all 0.3s;
}
.btn-outlined.btn-primary {
    background: none;
    border: 2px solid #428bca;
    color: #428bca;
}
.btn-outlined.btn-primary:hover,
.btn-outlined.btn-primary:active {
    color: #FFF;
    background: #428bca;
    border-color: #428bca:
}

.btn-outlined.btn-success {
    background: none;
    border: 2px solid #5cb85c;
    color: #5cb85c;
}
.btn-outlined.btn-success:hover,
.btn-outlined.btn-success:active {
    color: #FFF;
    background: #47a447;
}

.btn-outlined.btn-info {
    background: none;
    border: 2px solid #5bc0de;
    color: #5bc0de;
}
.btn-outlined.btn-info:hover,
.btn-outlined.btn-info:active {
    color: #FFF;
    background: #39b3d7;
}

.btn-outlined.btn-warning {
    background: none;
    border: 2px solid #f0ad4e;
    color: #f0ad4e;
}
.btn-outlined.btn-warning:hover,
.btn-outlined.btn-warning:active {
    color: #FFF;
    background: #ed9c28;
}

.btn-outlined.btn-danger {
    background: none;
    border: 2px solid #d9534f;
    color: #d9534f;
}
.btn-outlined.btn-danger:hover,
.btn-outlined.btn-danger:active {
    color: #FFF;
    background: #d2322d;
}
</style>
<div class="ossn-ads">
<div class="ossn-widget ">
<div class="widget-heading"><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Fresher">Fresher Jobs</a></div>
<ul>
<div class="jobs-right" style="margin-left: 0px; padding-bottom: 10px;">
            <p><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Fresher&jType=Referal%20Jobs" class="btn btn-outlined  btn-block btn-primary">Referral Jobs</a></p>
            <p><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Fresher&jType=Off-Campuses" class="btn btn-outlined  btn-block btn-success">Off Campus Jobs</a></p>
            <p><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Fresher&jType=Walkins" class="btn btn-outlined  btn-block btn-danger">Walkins</a></p>
            <p><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Fresher&jType=bpo" class="btn btn-outlined  btn-block btn-info">BPO Jobs</a></p>
            
           
        </div>
</ul>
</div>
</div>
<br>

<div class="ossn-widget ">
<div class="widget-heading"><a target="_blank" target="new" href="http://www.bjspi.com/job_office/jobList.php?type=Experienced">Experience  Jobs</a></div>
<ul>
<div class="jobs-right" style="margin-left: 0px; padding-bottom: 10px;">
            <p><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Experienced&domain=testing" class="btn btn-outlined  btn-block btn-primary">Testing Jobs</a></p>
            <p><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Experienced&domain=java" class="btn btn-outlined  btn-block btn-success">Java Jobs</a></p>
            <p><a target="_blank" href="http://www.bjspi.com/job_office/jobList.php?type=Experienced" class="btn btn-outlined  btn-block btn-danger">More Jobs ...</a></p>
           
        </div>
</ul>
</div>
</div></div>
<br>
               
			</div>
		</div>
       <?php } ?>  
	</div>
</div>