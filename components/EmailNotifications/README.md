EMAIL NOTIFICATIONS
====================
A utility that send email to user about his notification. 

The following notifications are available via email:

1. Wall post comment
2. Wall post like
2. Comment like
3. Profile photo Comment
4. Profile photo like
5. Album photo comment
6. Album photo like
8. Group membership request
9. Group wall post comment
10. Group wall post like