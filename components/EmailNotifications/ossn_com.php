<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
define('__EMAIL_NOTIFICATIONS__', ossn_route()->com . 'EmailNotifications/');

require_once(__EMAIL_NOTIFICATIONS__ . 'classes/EmailNotifications.php');

/**
 * Email Notification initialize
 *
 * @return void
 */
function email_notifications_init() {
		ossn_register_callback('notification', 'add', 'email_notification');
}
/**
 * Email notification construct
 *
 * @param string $text A main text
 * @param object $user A OssnUser
 * @param string $subject_url A URL for subject
 *
 * @return string|boolean
 */
function email_notification_construct($text, $user, $subject_url) {
		if(!empty($text) && $user) {
				$body = ossn_print("email:notfication:main:body", array(
						$user->fullname,
						$text,
						$subject_url,
						ossn_site_settings('site_name'),
						ossn_site_url()
				));
				return $body;
		}
		return false;
}
/**
 * Email notification
 *
 * @param string $callback A callback
 * @param string $type  A callback type
 * @param array  $params A option values
 *
 * @return void
 */
function email_notification($callback, $type, $params) {
		$poster = ossn_user_by_guid($params['poster_guid']);
		$owner  = ossn_user_by_guid($params['owner_guid']);
		if($poster && $owner) {
				$userURL = ossn_plugin_view('output/url', array(
						'text' => $poster->fullname,
						'href' => $poster->profileURL()
				));
				$text    = ossn_print("ossn:notifications:{$params['type']}", array(
						$userURL
				));
				
				switch($params['type']) {
						case 'comments:entity:file:profile:photo':
								$subject_url = ossn_site_url("photos/user/view/{$params['subject_guid']}");
								break;
						case 'like:post:group:wall':
						case 'like:post':
						case 'ossn_group_comment_post':
						case 'comments:post':
								$subject_url = ossn_site_url("post/view/{$params['subject_guid']}");
								break;
						case 'group:joinrequest':
								$subject_url = ossn_site_url("group/{$params['subject_guid']}/requests");
								break;
						case 'like:annotation':
								$result = ossn_get_entity($params['subject_guid']);
								if($result->subtype == 'file:ossn:aphoto') {
										$subject_url = ossn_site_url("photos/view/{$params['subject_guid']}");
								}
								if($result->subtype == 'file:profile:photo') {
										$subject_url = ossn_site_url("photos/user/view/{$params['subject_guid']}");
								}
								if($result->subtype == 'file:profile:cover') {
										$subject_url = ossn_site_url("photos/cover/view/{$params['subject_guid']}");
								}
								break;
						case 'comments:entity:file:ossn:aphoto':
						case 'ossn_notification_like_photo':
								$subject_url = ossn_site_url("photos/view/{$params['subject_guid']}");
								break;
						case 'ossnpoke:poke':
								$subject_url = $poster->profileURL();
								break;
				}
				
				$subject_url = ossn_plugin_view('output/url', array(
						'href' => $subject_url,
						'text' => $subject_url
				));
				$main        = email_notification_construct($text, $owner, $subject_url);
				$subject     = ossn_print("mail:notfication:subject");
				
				$mail = new EmailNotifications;
				$mail->NotifiyUser($owner->email, $subject, $main);
		}
}
ossn_register_callback('ossn', 'init', 'email_notifications_init');