<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

define('REMEMBER_LOGIN', ossn_route()->com . 'RememberLogin/');

/**
 * Remember Login Init
 * 
 * @return void
 */
function remember_login_init() {
	
		ossn_extend_view('js/opensource.socialnetwork', 'js/fingerprint');
		if(!ossn_isLoggedin()) {
			ossn_extend_view('forms/login2/before/submit', 'rememberlogin/checkbox');
			ossn_register_action('user/login', REMEMBER_LOGIN . 'actions/user/login.php');
			ossn_add_hook('page', 'load', 'remember_login_member_validation');
		}
		else {
			ossn_register_action('user/logout', REMEMBER_LOGIN . 'actions/user/logout.php');
			if (isset($_COOKIE['rl_vfp'])) {
				// at this stage, fingerprint has overwritten rl_bfp that may have been inserted by a hacker
				// thus, if cookies differ now, it was a hack attempt and user will be logged off
				if($_COOKIE['rl_vfp'] != $_COOKIE['rl_bfp']) {
					    unset($_COOKIE['rl_vfp']);
						setcookie('rl_vfp', '', time() - 3600, '/');
					    unset($_COOKIE['rl_uma']);
						setcookie('rl_uma', '', time() - 3600, '/');
						ossn_logout();
						redirect();
				}
			}
		}
}
/**
 * Remember Login Member Validation
 * 
 * @return void
 */
function remember_login_member_validation() {
		if(!ossn_isLoggedin() && isset($_COOKIE['rl_uma']) && isset($_COOKIE['rl_bfp'])) {
			// make fingerprint.js validate whether given rl_bfp is the REAL fingerprint of this browswer
			// when login is successful and user is forwarded to /home
			setcookie('rl_vfp', $_COOKIE['rl_bfp'], time() + (86400 * 30 * 12), "/");
			$uma_obj = trim(ossn_string_decrypt($_COOKIE['rl_uma'], $_COOKIE['rl_bfp'] . ossn_site_settings('site_key')));
			$uma = json_decode($uma_obj, true);
			if(!empty($_COOKIE['rl_uma']) && $uma['ua'] !== md5($_SERVER['HTTP_USER_AGENT'])){
						unset($_COOKIE['rl_vfp']);
						setcookie('rl_vfp', '', time() - 3600, '/');
					    unset($_COOKIE['rl_uma']);
						setcookie('rl_uma', '', time() - 3600, '/');
						ossn_logout();
			}
			if(filter_var($uma['email'], FILTER_VALIDATE_EMAIL)) {
				$ossnuser = ossn_user_by_email($uma['email']);
				if($ossnuser && !$ossnuser->isAdmin()) {
					OssnSession::assign('OSSN_USER', $ossnuser);
					redirect('home');
				}
			}
		}
}
/**
 * Rembember me data
 *
 * return string
 */
function rembember_me_data(){
		return json_encode(array(
					'email' => ossn_loggedin_user()->email,
					'ua' => md5($_SERVER['HTTP_USER_AGENT']),
		));	
}
ossn_register_callback('ossn', 'init', 'remember_login_init');