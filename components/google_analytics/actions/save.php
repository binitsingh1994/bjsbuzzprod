<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$OssnComponents = new OssnComponents;

$ga_code = input('ga_code');

$settings = array(
	'ga_code' => $ga_code
);

foreach ($settings as $key => $value){
	$success = $OssnComponents->setComSETTINGS('google_analytics', $key, $value);
}

if($success){
	ossn_trigger_message(ossn_print('com:google_analytics:settings:saved'));
    redirect(REF);
} else {
	ossn_trigger_message(ossn_print('com:google_analytics:settings:save:error'), 'error');
    redirect(REF);
}



