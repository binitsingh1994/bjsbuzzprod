<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
?>
<label><?php echo ossn_print('com:google_analytics:label');?></label>
<textarea class="google_analytics_textarea" rows="10" cols="100" name="ga_code" placeholder="<?php echo ossn_print('com:google_analytics:placeholder'); ?>"><?php echo $params['settings']->ga_code;?></textarea>
<input type="submit" class="ossn-admin-button button-dark-blue" value="<?php echo ossn_print('save'); ?>"/>