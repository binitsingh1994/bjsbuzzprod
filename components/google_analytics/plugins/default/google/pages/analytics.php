<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$settings = new OssnComponents;
$settings = $settings->getComSettings('google_analytics');
$ga_code  = $settings->ga_code;
?>

<!-- Google Analytics -->
<?php 
echo html_entity_decode($ga_code, ENT_QUOTES, 'UTF-8');
?>
<!-- End Google Analytics -->
