Google Analytics
================

V1.0
This component allows you to add Google Analytics code to your community.

Install and activate the component from your admin dashboard as usual
Proceed to Configure->Google Analytics
Paste the Javascript code you received from Google
Click the Save button

The analytics code will be inserted to every page of your site now.

