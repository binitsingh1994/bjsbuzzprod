<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$en = array(
	'google_analytics' => 'Google Analytics',
	'com:google_analytics:label' => 'Google Analytics script',
	'com:google_analytics:placeholder' => 'Enter or paste your Google code here',
	'com:google_analytics:settings:saved' => 'Script successfully saved',
	'com:google_analytics:settings:save:error' => 'Script cannot be saved',
);
ossn_register_languages('en', $en);