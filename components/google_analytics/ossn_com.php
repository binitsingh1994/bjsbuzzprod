<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

define('__GOOGLE_ANALYTICS__', ossn_route()->com . 'google_analytics/');

function google_analytics_init() {
	ossn_extend_view('ossn/admin/head', 'google_analytics_admin_head');

	ossn_register_com_panel('google_analytics', 'settings');

	ossn_extend_view('ossn/site/head','google/pages/analytics');
	
	if(ossn_isAdminLoggedin()){
		ossn_register_action('admin/google_analytics/settings', __GOOGLE_ANALYTICS__ . 'actions/save.php');		
	}	
}

function google_analytics_admin_head(){
	$head	 = array();	
	$head[]  = ossn_html_css(array(
					'href' => ossn_site_url() . 'components/google_analytics/plugins/default/css/google_analytics.css'
			  ));	
	return implode('', $head);
}

ossn_register_callback('ossn', 'init', 'google_analytics_init');