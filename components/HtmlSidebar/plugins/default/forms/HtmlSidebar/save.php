<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
 ?>
 <div>
	 <label>Enter HTML code here</label>
 	<textarea name="html"><?php $component = new OssnComponents;$settings  = $component->getSettings('HtmlSidebar');echo html_sidebar_output($settings->free_html);?></textarea>
 </div>
 <div>
 	<input type="submit" value="Save" class="btn btn-primary"/>
 </div>   