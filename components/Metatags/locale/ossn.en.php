<?php
/* Sathish Kumar S
 * @package Meta Tags 
 * @author Sathish Kumar S 
 */

$en = array(
	'meta:1' => 'Keywords for your website.',
	'meta:2' => 'Description for your website Example: Socail nework website.',
	'meta:3' => 'Website Author Example: Name of Webmaster.',
	'meta:4' => 'Robots is used by search engine to Crawling or Crawl your website pages Example: index,follow,noindex,nofollow,none,noarchive.',
	'meta:5' => 'After how many days Search engine RECrawl your website pages Example 1, 2 , 3, 5.',

);

ossn_register_languages('en', $en);
