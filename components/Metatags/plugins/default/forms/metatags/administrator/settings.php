<?php
/**
 *    OpenSource-SocialNetwork
 *
 * @package   http://www.webbehinds.com
 * @author    Sathish kumar S<info@opensource-socialnetwork.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES 
 * @license   General Public Licence http://opensource-socialnetwork.com/licence
 * @link      http://www.opensource-socialnetwork.com/licence
 */
?>
<label>Metatags</label>
<label><?php echo ossn_print('meta:1');?></label>
<input type="text" name="keywords" value="<?php echo $params['settings']->keywords;?>"/>
<label><?php echo ossn_print('meta:2');?></label>
<input type="text" name="description" value="<?php echo $params['settings']->description;?>"/>
<label><?php echo ossn_print('meta:3');?></label>
<input type="text" name="author" value="<?php echo $params['settings']->author;?>"/>
<label><?php echo ossn_print('meta:4');?></label>
<input type="text" name="robots" value="<?php echo $params['settings']->robots;?>"/>
<label><?php echo ossn_print('meta:5');?></label>
<input type="text" name="revisit" value="<?php echo $params['settings']->revisit;?>"/>
<input type="submit" class="ossn-admin-button button-dark-blue" value="<?php echo ossn_print('save'); ?>"/>