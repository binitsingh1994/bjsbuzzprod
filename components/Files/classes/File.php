<?php
/**
 * Open Source Social Network
 *
 * @package   Open Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
class File extends OssnObject {
		/** 
		 * Add file
		 * 
		 * @param string $desciption A detail info about file
		 * @param integer $owner_guid A owner guid
		 * @param string $owner_type A owner type
		 *
		 * @return boolean
		 */
		public function addFile($description = "", $owner_guid = "", $owner_type = "user") {
				if(empty($owner_guid)) {
						$owner_guid = ossn_loggedin_user()->guid;
				}
				$this->description = $description;
				$this->owner_guid  = $owner_guid;
				$this->type        = 'user';
				$this->subtype     = 'file';
				
				$this->data->owner_type = $owner_type;
				$this->data->owner_guid = $owner_guid;
				
				if($guid = $this->addObject()) {
						$object           = ossn_get_object($guid);
						$file             = new OssnFile;
						$file->type       = 'object';
						$file->owner_guid = $guid;
						$file->subtype    = 'File';
						$file->setFile('file');
						$file->setPath('file/contents/');
						$file->setMimeTypes(file_allowed_mime_types());
						$file->setExtension(file_allowed_mime_extensions());
						if(!$file->addFile()) {
								$object->deleteObject($object->guid);
								return false;
						}
						$object->title = strtolower($file->file['name']);
						$object->save();
						
						$this->addWall($desc, $owner_type, $owner_guid, $object->guid);
						return $guid;
				}
				return false;
		}
		/**
		 * Add video to wall
		 *
		 * @param integer $itemguid A video guid
		 *
		 * @return boolean
		 */
		public function addWall($desc = "null:data", $walltype = "user", $owner_guid = "", $itemguid = '') {
				if(empty($itemguid) || !class_exists("OssnWall")) {
						return false;
				}
				$this->wall              = new OssnWall;
				$this->wall->type        = $walltype;
				$this->wall->item_type   = 'file';
				$this->wall->owner_guid  = $owner_guid;
				$this->wall->poster_guid = $owner_guid;
				$this->wall->item_guid   = $itemguid;
				if($this->wall->Post('null:data')) {
						return true;
				}
				return false;
		}
		/**
		 * Get all Files
		 *
		 * @param array $params A options
		 *
		 * @return array
		 */
		public function getAll(array $params = array()) {
				$default             = array();
				$default['type']     = 'user';
				$default['subtype']  = 'file';
				$default['order_by'] = 'o.guid DESC';
				
				$vars = array_merge($default, $params);
				return $this->searchObject($vars);
		}
		/**
		 * Get extension of file
		 *
		 * @return string
		 */
		public function getExtension() {
				if(isset($this->title)) {
						$file = new OssnFile;
						return $file->getFileExtension($this->title);
				}
				return false;
		}
		/**
		 * Get icon URL
		 *
		 * @return string
		 */
		public function getIcon() {
				return ossn_site_url() . 'components/Files/images/icons/' . $this->getExtension() . '-icon-128x128.png';
		}
		/**
		 * Get file URL
		 *
		 * @return string
		 */
		public function getURL() {
				return ossn_site_url() . 'files/view/' . $this->guid;
		}
		/**
		 * Get file download URL
		 *
		 * @return string
		 */
		public function getDownloadURL() {
				return ossn_site_url() . 'files/download/' . $this->guid;
		}
		/**
		 * Get file delete URL
		 *
		 * @return string
		 */
		public function deleteURL() {
				return ossn_site_url('action/file/delete?guid=' . $this->guid, true);
		}
		/**
		 * Remove file data
		 *
		 * @return void
		 */
		public function removeData() {
				$guid   = $this->guid;
				$entity = ossn_get_entities(array(
						'type' => 'object',
						'owner_guid' => $guid,
						'subtype' => 'file:File'
				));
				if(class_exists('OssnNotifications') && !empty($entity[0]->guid)) {
						$ndelete = new OssnNotifications;
						$ndelete->deleteNotification(array(
								'subject_guid' => $entity[0]->guid,
								'type' => array(
										'like:entity:file:File',
										'comments:entity:file:File'
								)
						));
				}
				if(class_exists('OssnComments') && !empty($entity[0]->guid)) {
						$comments     = new OssnComments;
						$comments_all = $comments->GetComments($entity[0]->guid, 'entity');
						foreach($comments_all as $comment) {
								$comment->deleteComment($comment->id);
						}
				}
				if(class_exists('OssnLikes') && !empty($entity[0]->guid)) {
						$likes     = new OssnLikes;
						$likes_all = $likes->deleteLikes($entity[0]->guid, 'entity');
				}
				if(class_exists('OssnWall') && !empty($guid)) {
						$wall       = new OssnWall;
						$wall_posts = $wall->searchObject(array(
								'subtype' => 'wall',
								'entities_pairs' => array(
										array(
												'name' => 'item_type',
												'value' => 'file'
										),
										array(
												'name' => 'item_guid',
												'value' => $guid
										)
								)
						));
						if($wall_posts) {
								foreach($wall_posts as $item) {
										$item->deletePost($item->guid);
								}
						}
				}
		}
}