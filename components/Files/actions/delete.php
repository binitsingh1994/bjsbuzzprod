<?php
$guid = input('guid');
$file = ossn_file_get($guid);
if(!$file){
	ossn_trigger_message(ossn_print("file:delete:failed"), "error");
	redirect(REF);
}
if($file->owner_guid == ossn_loggedin_user()->guid || ossn_loggedin_user()->canModerate()){
		$file->removeData();
		if($file->deleteObject()){
				ossn_trigger_message(ossn_print("file:deleted"));
				redirect("files/all");
		}	
}
ossn_trigger_message(ossn_print("file:delete:failed"), "error");
redirect(REF);
