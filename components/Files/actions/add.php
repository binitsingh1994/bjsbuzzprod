<?php
$desc = input('desc');
$file = new File;
if(!isset($_FILES['file']) || $_FILES['file']['error'] !== UPLOAD_ERR_OK){
		ossn_trigger_message(ossn_print('file:add:failed:1'), 'error');
		redirect(REF);	
}

if($guid = $file->addFile($desc)){
		ossn_trigger_message(ossn_print('file:added'));
		redirect("files/view/{$guid}");
} else {
		ossn_trigger_message(ossn_print('file:add:failed'), 'error');
		redirect(REF);
}