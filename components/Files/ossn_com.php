<?php
/**
 * Open Source Social Network
 *
 * @package   Open Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
define('FILES', ossn_route()->com . 'Files/');
require_once(FILES . 'classes/File.php');
/**
 * Initialize the system
 *
 * @return void
 */
function ossn_files_init() {
		ossn_extend_view('css/ossn.default', 'css/files');
		
		ossn_register_page('files', 'ossn_files_page_handler');
		if(ossn_isLoggedin()) {
				ossn_register_action('file/delete', FILES . 'actions/delete.php');
				ossn_register_action('file/add', FILES . 'actions/add.php');
		}
		ossn_add_hook('wall:template', 'file', 'ossn_wall_file');
		
		ossn_add_hook('notification:view', 'comments:entity:file:File', 'ossn_notification_file_comment_like_view');
		ossn_add_hook('notification:view', 'like:entity:file:File', 'ossn_notification_file_comment_like_view');
		ossn_add_hook('notification:view', 'like:annotation', 'ossn_notification_file_comment_like_view');
		
		$user = ossn_loggedin_user();
		ossn_register_sections_menu('newsfeed', array(
				'name' => 'files_my',
				'text' => ossn_print('files:com:my'),
				'url' => ossn_site_url('files/all/') . $user->guid,
				'section' => 'files',
				'icon' => true
		));
		ossn_register_sections_menu('newsfeed', array(
				'name' => 'files_all',
				'text' => ossn_print('files:com:all'),
				'url' => ossn_site_url('files/all/'),
				'section' => 'files',
				'icon' => true
		));
		ossn_register_sections_menu('newsfeed', array(
				'name' => 'files_add',
				'text' => ossn_print('files:com:add'),
				'url' => ossn_site_url('files/add'),
				'section' => 'files',
				'icon' => true
		));
		ossn_register_callback('user', 'delete', 'ossn_user_files_delete');
}
/**
 * Delete user files  when user deleted
 *
 * @return void;
 * @access private
 */
function ossn_user_files_delete($callback, $type, $params) {
		$guid  = $params['entity'];
		$files = new Files;
		$all   = $files->getAll(array(
				'owner_guid' => $guid,
				'page_limit' => false
		));
		if($all && !empty($guid)) {
				foreach($all as $item) {
						$item->removeData();
						$item->deleteObject();
				}
		}
}
/**
 * Files notification view for comment and like
 *
 * @param string $hook A hook name
 * @param string $type A hook type
 * @param string $return A mixed data
 * @param object $params A option values
 *
 * @return mixed data
 */
function ossn_notification_file_comment_like_view($hook, $type, $return, $params) {
		$notif = $params;
		
		$entity  = ossn_get_entity($notif->subject_guid);
		$subject = ossn_file_get($entity->owner_guid);
		if($subject && (($notif->type == 'like:annotation' && $subject->subtype == 'file') || $notif->type == 'comments:entity:file:File' || $notif->type == 'like:entity:file:File')) {
				$baseurl        = ossn_site_url();
				$user           = ossn_user_by_guid($notif->poster_guid);
				$user->fullname = "<strong>{$user->fullname}</strong>";
				$iconURL        = $user->iconURL()->small;
				
				$img  = "<div class='notification-image'><img src='{$iconURL}' /></div>";
				$type = "<div class='ossn-notification-icon-calander'></div>";
				if($notif->viewed !== NULL) {
						$viewed = '';
				} elseif($notif->viewed == NULL) {
						$viewed = 'class="ossn-notification-unviewed"';
				}
				
				$url               = $subject->getURL();
				$notification_read = "{$baseurl}notification/read/{$notif->guid}?notification=" . urlencode($url);
				return "<a href='{$notification_read}'>
	       <li {$viewed}> {$img} 
		   <div class='notfi-meta'> {$type}
		   <div class='data'>" . ossn_print("ossn:notifications:{$notif->type}", array(
						$user->fullname
				)) . '</div>
		   </div></li>';
		}
}
/**
 * Template for wall file
 *
 * @return mixed data;
 */
function ossn_wall_file($hook, $type, $return, $params) {
		return ossn_plugin_view("files/wall", $params);
}
/**
 * Files page handler
 *
 * @return void
 */
function ossn_files_page_handler($pages) {
		switch($pages[0]) {
				case 'add':
						$title               = ossn_print('file:add');
						$contents['content'] = ossn_plugin_view('files/add');
						$content             = ossn_set_page_layout('newsfeed', $contents);
						echo ossn_view_page($title, $content);
						break;
				case 'all':
						$guid                = $pages[2];
						$title               = ossn_print('file:all');
						$contents['content'] = ossn_plugin_view('files/all', array(
								'guid' => $guid
						));
						$content             = ossn_set_page_layout('newsfeed', $contents);
						echo ossn_view_page($title, $content);
						break;
				case 'view':
						$file = ossn_file_get($pages[1]);
						if($file) {
								$title               = $file->title;
								$contents['content'] = ossn_plugin_view('files/view', array(
										'file' => $file
								));
								$content             = ossn_set_page_layout('newsfeed', $contents);
								echo ossn_view_page($title, $content);
						} else {
								ossn_error_page();
						}
						break;
				case 'download':
						$file = ossn_file_get($pages[1]);
						if($file) {
								$entity = ossn_get_entities(array(
										'type' => 'object',
										'owner_guid' => $file->guid,
										'subtype' => 'file:File'
								));
								if(isset($entity[0]->guid)) {
										$pfile = ossn_get_file($entity[0]->guid);
										$path  = $pfile->getPath();
										if(!is_file($path)) {
												goto showerror;
										}
										ob_flush();
										header("Content-Description: File Transfer");
										header("Content-Type: application/octet-stream");
										header("Content-Disposition: attachment; filename='" . basename($file->title) . "'");
										readfile($path);
										exit;
								}
						} else {
								goto showerror;
						}
						showerror:
						ossn_error_page();
						break;
				default:
						ossn_error_page();
		}
}
/**
 * Get a file
 *
 * @param integer $guid A file guid
 *
 * @return object|boolean
 */
function ossn_file_get($guid) {
		if(!empty($guid)) {
				$object = ossn_get_object($guid);
				if($object->subtype == 'file') {
						$object = (array) $object;
						return arrayObject($object, 'File');
				}
		}
		return false;
}
/**
 * File supported types
 *
 * @return array
 */
function file_allowed_mime_types() {
		return array(
				'docx' => array(
						'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
				),
				'pdf' => array(
						'application/pdf'
				),
				'doc' => array(
						'application/msword'
				),
				'mp3' => array(
						'audio/mpeg'
				),
				'mp4' => array(
						'video/mp4'
				),
				'zip' => array(
						'application/zip'
				),
				'xlsx' => array(
						'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
				),
				'xls' => array(
						'application/vnd.ms-excel'
				)
		);
}
/**
 * File supported extensions
 *
 * @return array
 */
function file_allowed_mime_extensions() {
		$types = file_allowed_mime_types();
		foreach($types as $key => $item) {
				$list[] = $key;
		}
		return $list;
}
ossn_register_callback('ossn', 'init', 'ossn_files_init');