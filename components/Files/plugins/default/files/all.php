<div class="ossn-page-contents file-view-page-contents">
	<?php
	$files = new File;
	
	if(empty($params['guid'])){
		$all = $files->getAll();
		$count = $files->getAll(array('count' => true));
	} else {
		$all = $files->getAll(array(
				'owner_guid' => $params['guid']
		));
		$count = $files->getAll(array('count' => true));		
	}
	if($all){
		foreach($all as $item){
				$data['file'] = $item;
				$data['user_entity'] = false;
				$data['show_desc'] = false;
				echo ossn_plugin_view('files/item', $data);
		}
	}
	echo ossn_view_pagination($count);
	?>
</div>