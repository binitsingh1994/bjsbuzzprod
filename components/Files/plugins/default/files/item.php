<?php $user = ossn_user_by_guid($params['file']->owner_guid);
	if(!isset($params['show_desc'])){
		$params['show_desc'] = true;	
	}
?>
<div class="file-view-item">
	<?php if($params['user_entity'] != false){ ?>
	<div class="user-entity">
	    <div class="row">
    		<div class="col-md-1">
        			<img src="<?php echo $user->iconURL()->small;?>" />
        	</div>
        	<div class="col-md-11">
        		<div class="user-name">
            			<a href="<?php echo $user->profileURL();?>"><?php echo $user->fullname;?></a>
                        <?php
						if(ossn_isLoggedin() && $params['file']->owner_guid == ossn_loggedin_user()->guid || ossn_isLoggedin() && ossn_loggedin_user()->canModerate()){ ?>
                        <a href="<?php echo $params['file']->deleteURL();?>" class="label label-danger right ossn-make-sure"><?php echo ossn_print('delete');?></a>
                        <?php } ?>
            	</div>	
        	</div>
    	</div>
    </div>  
    <?php }?>  
    <?php if($params['show_desc'] == true){ ?>
	<p><?php echo $params['file']->description;?></p>
	<?php } ?>
    <div class="file-container">
    <div class="row">
	    	<div class="col-md-2 icon-container">
    	    	<img src="<?php echo $params['file']->getIcon();?>" />
        	</div>
        	<div class="col-md-10">
  			     <p class="file-title"><a href="<?php echo $params['file']->getURL();?>"><?php echo $params['file']->title;?></a></p>
				 <span class="time-created"><?php echo ossn_user_friendly_time($params['file']->time_created); ?></span>
                 <p class="btns"><a target="_blank" href="<?php echo $params['file']->getDownloadURL();?>" class="btn-action"><?php echo ossn_print('file:download');?></a></p>
        	</div>
    </div>
    </div>
</div>