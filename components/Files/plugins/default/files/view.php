<div class="ossn-page-contents file-view-page-contents">
	<?php
	$params['user_entity'] = true;
	echo ossn_plugin_view('files/item', $params);?>
 	<div class="file-view-comments-likes">
	<?php
		$entity = ossn_get_entities(array(
			'type' => 'object',
			'owner_guid' => $params['file']->guid,
			'subtype' => 'file:File',
  		));	
		$vars['entity'] = $entity[0];
		echo ossn_plugin_view('entity/comment/like/share/view', $vars);
	?>
    </div>
</div>