<?php
/**
 * Open Source Social Network
 *
 * @package   Open Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
 echo "<div class='ossn-page-contents'>";
 $form = ossn_view_form('file/add', array(
		'id' => 'file-add',
		'action' => ossn_site_url('action/file/add'),
 ));
 echo ossn_plugin_view('widget/view', array(
		'title' => ossn_print('file:add'),
		'contents' => $form,
 ));
 echo "</div>"; 