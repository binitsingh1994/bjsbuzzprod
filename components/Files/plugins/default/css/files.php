.file-view-item {
	margin-bottom:10px;
}
.file-view-item .file-container {
	border: 1px solid #eee;
    padding: 10px;
}
.file-view-item .icon-container {
    border-right: 1px solid #eee;	
}
.file-view-item img {
	width: 100%;
}
.ossn-wall-item .post-contents .file-view-item img {
	border:none;
	margin-bottom: 0px;
}
.file-view-item .btns {
    margin-top: 8px;
    display: block;
}
.file-view-item .file-title a,
.file-view-item .file-title {
    font-weight: bold;
    color: #5a5a5a;
    margin-bottom: 1px;
}
.file-view-item .user-name a {
	font-weight:bold;
}
.file-view-item .user-entity {
	border-bottom:1px solid #eee;
	padding-bottom:5px;
	margin-bottom:5px;
}
.file-view-comments-likes .like-share,
.file-view-comments-likes .comments-list{
	margin-left: -10px;
    margin-right: -10px;
}
.file-view-page-contents {
	padding-bottom:0px;	
}
.menu-section-item-files-all:before,
.menu-section-item-files-my:before,
.menu-section-files i:before {
	content: "\f15b" !important;
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
}

.menu-section-item-files-add:before {
	content: "\f055" !important;
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
}