<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
 ?>
 <div>
 	<label><?php echo ossn_print('file:desc');?></label>
 	<textarea name="desc" placeholder="File description (optional)"></textarea>
 </div>
 <div>
 	<label><?php echo ossn_print('file:select');?> (<?php echo implode(',', file_allowed_mime_extensions());?>) </label>
    <input type="file" name="file" />
 </div>
 <div>
 	<input type="submit" value="<?php echo ossn_print('save');?>" class="btn btn-success" />
 </div>