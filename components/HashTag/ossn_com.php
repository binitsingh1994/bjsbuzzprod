<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   SOFTLAB24 LIMITED, COMMERCIAL LICENSE v1.0 https://www.softlab24.com/license/commercial-license-v1
 * @link      https://www.softlab24.com
 */
 
function hash_tag_init(){
	ossn_add_hook('wall', 'templates:item', 'ossn_hashtag', 150);
	ossn_add_hook('comment:view', 'template:params', 'ossn_hashtag', 150);	
	ossn_register_page('hashtag', 'hashtag_page_handler');
}
/**
 * Hashtag page handler
 *
 * @param string $text A hashtag 
 *
 * @return string
 */
function hashtag_page_handler($pages){
	if(!ossn_isLoggedin()){
			redirect('login');
	}
	$pages[0] = ossn_input_escape($pages[0]);
	$title = '#'.$pages[0];
	if(com_is_active('OssnWall') && !empty($pages[0])) {
			$contents['content'] = ossn_plugin_view('hashtag/view', array(
				'hashtag' => trim('#'.$pages[0]),															  
			));
	}
	$content = ossn_set_page_layout('newsfeed', $contents);
	echo ossn_view_page($title, $content);	
}
/**
 * Make the hashtag into to clickable events
 *
 * @param string $text A hashtag 
 *
 * @return string
 */
function hashtag($text){
	$url = ossn_site_url("hashtag/");
	return preg_replace('/(?<=\s|^)#([^\d_\s\W][\p{L}\d]{2,})/', '<a class="ossn-hashtag-item" href="'.$url.'$1">#$1</a>', $text);	
}
/**
 * Init hashtags in wall posts
 *
 * @note Please don't call this function directly in your code.
 * 
 * @param string $hook Name of hook
 * @param string $type Hook type
 * @param array|object $return Array or Object
 * @params array $params Array contatins params
 *
 * @return array
 * @access private
 */
function ossn_hashtag($hook, $type, $return, $params){
	$params['text'] = hashtag($return['text']);
	return $params;
}
ossn_register_callback('ossn', 'init', 'hash_tag_init');