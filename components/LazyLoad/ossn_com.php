<?php
define('__OSSN_LAZY_LOAD__', ossn_route()->com . 'LazyLoad/');

function ossn_lazy_load() {
  ossn_register_plugins_by_path(__OSSN_LAZY_LOAD__.'plugins/');  
  ossn_extend_view('js/opensource.socialnetwork', 'js/lazyload');
  ossn_extend_view('css/ossn.default', 'css/lazyload');
  
}
ossn_register_callback('ossn', 'init', 'ossn_lazy_load');
