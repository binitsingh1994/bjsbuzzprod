if(window.location.href.indexOf("home") > -1) {
    Ossn.RegisterStartupFunction(function() {
    $(document).ready(function() {
        /* 
        * Create definitions for containers and selectors in which we 
        * will replace the pagination with scrolling.
        */
        
        var params=[];
    
        /* Newsfeed */
        params.push({
            items_Container:'.user-activity',
            item_Entity:'.ossn-wall-item',
        });
    
        /* Define the scroller class/object */
        LazyLoad = {
            site_base_url:       '<?php echo ossn_site_url($extend = '',$action = false); ?>',
            tab_header:          '.panel-heading',         
            tab_container:       '.tab-content',
            items_Container:     '.user-activity',
            pagination_item:     '.newsfeed-middle',
            item_Entity:         '.ossn-wall-item',
            pagination_Container:'.container-table',
            pagination_Selector: '.pagination.ossn-pagination',
            current_Selector:    'li.active',
            newsfeed:            {container:null, active:false, lock:false, width:0, height:0, bottom: 0},
            loadingHtml:         '<div class="hs-loading"><div class="timeline-item"><div class="animated-background facebook"><div class="background-masker header-top"></div><div class="background-masker header-left"></div><div class="background-masker header-right"></div><div class="background-masker header-bottom"></div><div class="background-masker subheader-left"></div><div class="background-masker subheader-right"></div><div class="background-masker subheader-bottom"></div><div class="background-masker content-top"></div><div class="background-masker content-first-end"></div><div class="background-masker content-second-line"></div><div class="background-masker content-second-end"></div><div class="background-masker content-third-line"></div><div class="background-masker content-third-end"></div></div></div></div></div>',
            loadingClass:        '.hs-loading',
            back2TopHtml:        '<div class="hs-back2top"><i class="fa fa-angle-double-up"></i></div>',
            show:                function(elems) { elems.show(); },
            nextPageUrl:         null,
            nextWalkinPageUrl:   null,
            scrolltimeout:       null,
            throttle:            110,

            init: function(options) {
                /* Check if there are other arguments aside from default */
                for (var key in options) {
                    /* Set the argument to our object */
                    LazyLoad[key] = options[key];
                }
                
                /* check the current page for presence of the required elements */
                if (($('body').find(LazyLoad.pagination_Selector).length) &&
                    ($('body').find(LazyLoad.items_Container).length && $('body').find(LazyLoad.item_Entity).length) 
                ){
                    LazyLoad.newsfeed.container = $(LazyLoad.newsfeed.selector);
                    if (LazyLoad.newsfeed.container.length){
                        LazyLoad.newsfeed.active = true;
                        LazyLoad.newsfeed.width = LazyLoad.newsfeed.container.outerWidth();
                        LazyLoad.newsfeed.height = LazyLoad.newsfeed.container.outerHeight();
                    }
                
                    /* Bind the scrolling event to our function */
                    $(function(){
                        $('body').css('height','auto');  
                        LazyLoad.back2top('init');
                        LazyLoad.getNextPageUrl($('body'),LazyLoad.activeContainer());
                        $(window).bind('scroll', LazyLoad.scroll);
                        LazyLoad.scroll();
                    });
                }
            },

            activeContainer: function(){
                return $(LazyLoad.tab_header).find('.active').find('a').attr('href');           
            },

            activeContentTab: function(){
                return $(LazyLoad.tab_container).find('.active').attr('id');
            },

            scroll: function() {
                if (!LazyLoad.scrollTimeout) {
                    LazyLoad.scrollTimeout = setTimeout(function () {
                        /* Load the next page if we reached the last item of the current page elements. */
                        if (LazyLoad.nearBottom()) {
                            var activeTab = LazyLoad.activeContainer();             
                            switch(activeTab){
                                case '#userpost':
                                    if(LazyLoad.shouldLoadNextPage()){
                                        LazyLoad.loadNextPage();
                                    }
                                    break;
                                case '#walkin':
                                    if(LazyLoad.shouldLoadWalkinNextPage()){
                                        LazyLoad.loadNextPage();
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        LazyLoad.scrollTimeout = null;
                    }, LazyLoad.throttle);
                }           
            },

            nearBottom: function() {
                /* Get the offset of our cursor in relation to the last element of the paged items */
                var scrollTop = $(window).scrollTop(),
                    windowHeight = $(window).height(),
                    lastItemOffset = $(LazyLoad.pagination_item).find(LazyLoad.activeContainer()).find(LazyLoad.item_Entity).last().offset();
                var scrollBottom = (scrollTop+windowHeight);
                if (scrollTop > (windowHeight/2)){
                    LazyLoad.back2top('show'); 
                } else {
                    LazyLoad.back2top('hide'); 
                }
                if (LazyLoad.newsfeed.active){
                    if (scrollBottom > LazyLoad.newsfeed.height){
                        var footer=$('footer').offset();
                        if (scrollBottom > footer.top){
                            LazyLoad.newsfeed.bottom = scrollBottom - footer.top;
                        } else {
                            LazyLoad.newsfeed.bottom = 0;
                        }
                        LazyLoad.newsfeedScroll('lock'); 
                    } else {
                        LazyLoad.newsfeedScroll('release'); 
                    }
                    
                }
                if (!lastItemOffset) return;
                return (scrollBottom > lastItemOffset.top );
            },

            shouldLoadNextPage: function() {
                /* check if there are succeeding pages still to load */
                return !!LazyLoad.nextPageUrl; 
            },
            
            shouldLoadWalkinNextPage: function() {
                return !!LazyLoad.nextWalkinPageUrl;
            },

            loadNextPage: function() {
                /* load the next page after the current page by loading the DOM object of the next page URL */
                var activeTab = LazyLoad.activeContainer(); 
                var nextPageUrl = LazyLoad.nextPageUrl;
                var nextWalkinPageUrl = LazyLoad.nextWalkinPageUrl;
                loading = $(LazyLoad.loadingHtml);
                
                LazyLoad.nextPageUrl = null;
                LazyLoad.nextWalkinPageUrl = null;
                /* Let's inform the user that we are currently working */
                loading.appendTo($(LazyLoad.tab_container).find(activeTab));
                /* Get the DOM specified by the next page URL */    
                
                switch(activeTab){
                    case '#userpost':
                        nextPageUrl = LazyLoad.site_base_url+activeTab.substr(1)+nextPageUrl;
                        break;
                    case '#walkin':
                        nextPageUrl = LazyLoad.site_base_url+activeTab.substr(1)+nextWalkinPageUrl;
                        break;
                    default:
                        break;
                }
                $.get(nextPageUrl, function(html) {
                    /* html now contains the DOM object */
                    var dom = $(html);
                    var items = dom.find(LazyLoad.pagination_item).find(LazyLoad.item_Entity);
                    var pagination_items = dom.find(LazyLoad.pagination_item).find(LazyLoad.pagination_Container); 
                    
                    if (items.find('.ossn-comment-attach-photo').length) {
                        LazyLoad.emojiis(items);
                    }
                    
                    //loading.remove();
                    $(LazyLoad.loadingClass).remove();

                    /* we now have the content of the succeeding page stored in 'items'. Let's add it to the container */
                    LazyLoad.show(items.hide().appendTo($(LazyLoad.tab_container).find(activeTab)));
                    LazyLoad.show(pagination_items.hide().appendTo($(LazyLoad.tab_container).find(activeTab)));
                    
                    /* On to the next page. */
                    LazyLoad.getNextPageUrl(dom,activeTab);
                    LazyLoad.scroll();
                });
            },
            
            getNextPageUrl: function(container,activeTab) {
                /* Check if there are succeeding pages after the current one */
                
                var pagination = $('html');
                var currentPageUrl;
                var currentwalkinPageUrl;

                if(LazyLoad.nextPageUrl == null && LazyLoad.nextWalkinPageUrl == null){
                    currentPageUrl = pagination.find('#userpost').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).find('a').attr('href');

                    LazyLoad.nextPageUrl = pagination.find('#userpost').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).next().find('a').attr('href');
                    
                    currentwalkinPageUrl = pagination.find('#walkin').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).find('a').attr('href');

                    LazyLoad.nextWalkinPageUrl = pagination.find('#walkin').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).next().find('a').attr('href');
                }

                switch(activeTab){
                    case '#userpost' : 
                        currentPageUrl = pagination.find('#userpost').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).find('a').attr('href');

                        LazyLoad.nextPageUrl = pagination.find('#userpost').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).next().find('a').attr('href');
                        break;
                    case '#walkin' :
                        currentwalkinPageUrl = pagination.find('#walkin').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).find('a').attr('href');

                        LazyLoad.nextWalkinPageUrl = pagination.find('#walkin').find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).next().find('a').attr('href');
                        break;
                    default:
                        break;
                }
                
            /* If the next page is still the current page, it means we have reached the last page. */
                if (currentwalkinPageUrl == LazyLoad.nextWalkinPageUrl){
                    /* No more page to load. Blank out the next page URL. */
                    LazyLoad.nextWalkinPageUrl=null;
                }

                if(currentPageUrl == LazyLoad.nextPageUrl){
                    LazyLoad.nextPageUrl = null;
                }
                /* Remove the pagination buttons */
                pagination.find('#'+activeTab.substr(1)).find(LazyLoad.pagination_Container).remove();
            },
            
            back2top: function(action){
                var backtoTop = $('body').find('.hs-back2top');
                if (action=='init'){
                    backtoTop = $(LazyLoad.back2TopHtml),
                    backtoTop.insertBefore($('footer'));
                
                    /* Unbind/Bind the mouse click to our function */
                    $(function(){
                        $('.hs-back2top').off('click');
                        $('.hs-back2top').on('click', function(){
                            $('html, body').animate({
                                scrollTop: $("body").offset().top
                            }, 300);
                        });
                    });
                }
                if ( ($('div.ossn-page-loading-annimation').css('display')) == 'none'){
                    if ( ((action=='show') || (action=='hide')) && (backtoTop.hasClass('hs-back2top')) ){
                        if (action=='show'){
                            backtoTop.css('bottom','5px');
                        } else {
                            backtoTop.css('bottom','-100px');
                        }
                    }
                }
            },
            
            newsfeedScroll: function(action){
                if (action=='lock'){
                    if (!LazyLoad.newsfeed.lock){
                        LazyLoad.newsfeed.lock = true;
                        $(LazyLoad.newsfeed.container).css({
                            position:'fixed',
                            width:LazyLoad.newsfeed.width+'px',
                            bottom:LazyLoad.newsfeed.bottom+'px'
                        });
                    } else {
                        $(LazyLoad.newsfeed.container).css({
                            bottom:LazyLoad.newsfeed.bottom+'px'
                        });
                    }
                } else if (action=='release' && LazyLoad.newsfeed.lock){
                    LazyLoad.newsfeed.lock = false;
                    $(LazyLoad.newsfeed.container).css({position:'relative',bottom:'auto'});
                }
            },

            
            emojiis: function(items){
                var emojii = {
                    icon:$(LazyLoad.items_Container).find('.ossn-comment-attach-photo i.fa-smile-o').parent().first(),
                    dropdown:$(LazyLoad.items_Container).find('.dropdown.emojii-container-main').first()
                };
                if (!items.find('.ossn-comment-attach-photo i.fa-smile-o').length){
                    $(emojii.icon).clone().insertAfter(items.find('.ossn-comment-attach-photo'));
                    $(emojii.dropdown).clone().appendTo(items.find('.comment-container'));
                }
            },

        }
    
        var len = params.length;
        for (var i = 0; i < len; i++) {
            var args=params[i];
            if (($('body').find(args.items_Container).length) && ($('body').find(args.item_Entity).length)){
                LazyLoad.init(args);
                break;
            }
        }
    });
});
}
else{
    Ossn.RegisterStartupFunction(function() {
    $(document).ready(function() {
    
        var params=[];
    
        /* Search Results - Users */
        params.push({
            items_Container:'.ossn-search-page .search-data',
            item_Entity:'.ossn-users-list-item',
        });
        params.push({
            items_Container:'.newsfeed-middle',
            item_Entity:'.ossn-wall-item',
        });

        params.push({
            items_Container:'.module-contents',
            item_Entity:'.ossn-users-list-item',
        });

        params.push({
            items_Container:'.user-activity',
            item_Entity:'.ossn-wall-item',
        });
    
        /* Define the scroller class/object */
        LazyLoad = {
            items_Container:     '.user-activity',
            item_Entity:         '.ossn-wall-item',
            pagination_Container:'.container-table',
            pagination_Selector: '.pagination.ossn-pagination',
            current_Selector:    'li.active',
            newsfeed:            {container:null, active:false, lock:false, width:0, height:0, bottom: 0},
            loadingHtml:         '<div class="hs-loading"><div class="timeline-item"><div class="animated-background facebook"><div class="background-masker header-top"></div><div class="background-masker header-left"></div><div class="background-masker header-right"></div><div class="background-masker header-bottom"></div><div class="background-masker subheader-left"></div><div class="background-masker subheader-right"></div><div class="background-masker subheader-bottom"></div><div class="background-masker content-top"></div><div class="background-masker content-first-end"></div><div class="background-masker content-second-line"></div><div class="background-masker content-second-end"></div><div class="background-masker content-third-line"></div><div class="background-masker content-third-end"></div></div></div></div></div>',
            loadingClass:        '.hs-loading',
            back2TopHtml:        '<div class="hs-back2top"><i class="fa fa-angle-double-up"></i></div>',
            show:                function(elems) { elems.show(); },
            nextPageUrl:         null,
            scrolltimeout:       null,
            throttle:            110,

            init: function(options) {
                /* Check if there are other arguments aside from default */
                for (var key in options) {
                    /* Set the argument to our object */
                    LazyLoad[key] = options[key];
                }
                
                /* check the current page for presence of the required elements */
                if (($('body').find(LazyLoad.pagination_Selector).length) &&
                    ($('body').find(LazyLoad.items_Container).length && $('body').find(LazyLoad.item_Entity).length) 
                ){
                    LazyLoad.newsfeed.container = $(LazyLoad.newsfeed.selector);
                    if (LazyLoad.newsfeed.container.length){
                        LazyLoad.newsfeed.active = true;
                        LazyLoad.newsfeed.width = LazyLoad.newsfeed.container.outerWidth();
                        LazyLoad.newsfeed.height = LazyLoad.newsfeed.container.outerHeight();
                    }
                
                    /* Bind the scrolling event to our function */
                    $(function(){
                        $('body').css('height','auto');  
                        LazyLoad.back2top('init');
                        LazyLoad.getNextPageUrl($('body'));
                        $(window).bind('scroll', LazyLoad.scroll);
                        LazyLoad.scroll();
                    });
                }
            },

            scroll: function() {
                if (!LazyLoad.scrollTimeout) {
                    LazyLoad.scrollTimeout = setTimeout(function () {
                        /* Load the next page if we reached the last item of the current page elements. */
                        if (LazyLoad.nearBottom() && LazyLoad.shouldLoadNextPage()) {
                            LazyLoad.loadNextPage();
                        }
                        LazyLoad.scrollTimeout = null;
                    }, LazyLoad.throttle);
                }           
            },

            nearBottom: function() {
                /* Get the offset of our cursor in relation to the last element of the paged items */
                var scrollTop = $(window).scrollTop(),
                    windowHeight = $(window).height(),
                    lastItemOffset = $(LazyLoad.items_Container).find(LazyLoad.item_Entity).last().offset();
                var scrollBottom = (scrollTop+windowHeight);
                if (scrollTop > (windowHeight/2)){
                    LazyLoad.back2top('show'); 
                } else {
                    LazyLoad.back2top('hide'); 
                }
                if (LazyLoad.newsfeed.active){
                    if (scrollBottom > LazyLoad.newsfeed.height){
                        var footer=$('footer').offset();
                        if (scrollBottom > footer.top){
                            LazyLoad.newsfeed.bottom = scrollBottom - footer.top;
                        } else {
                            LazyLoad.newsfeed.bottom = 0;
                        }
                        LazyLoad.newsfeedScroll('lock'); 
                    } else {
                        LazyLoad.newsfeedScroll('release'); 
                    }
                    
                }
                
                
                if (!lastItemOffset) return;
                return (scrollBottom > lastItemOffset.top );
            },

            shouldLoadNextPage: function() {
                /* check if there are succeeding pages still to load */
                return !!LazyLoad.nextPageUrl; 
            },
    
            loadNextPage: function() {
                /* load the next page after the current page by loading the DOM object of the next page URL */
                var nextPageUrl = LazyLoad.nextPageUrl,
                    loading = $(LazyLoad.loadingHtml);
                
                LazyLoad.nextPageUrl = null;
                /* Let's inform the user that we are currently working */
                loading.appendTo(LazyLoad.items_Container);
                /* Get the DOM specified by the next page URL */
                $.get(nextPageUrl, function(html) {
                    /* html now contains the DOM object */
                    var dom = $(html);
                    var items = dom.find(LazyLoad.items_Container).find(LazyLoad.item_Entity); 
                    if (items.find('.ossn-comment-attach-photo').length) {
                        LazyLoad.emojiis(items);
                    }
                    loading.remove();
                    /* we now have the content of the succeeding page stored in 'items'. Let's add it to the container */
                    LazyLoad.show(items.hide().appendTo(LazyLoad.items_Container));
                    
                    /* On to the next page. */
                    LazyLoad.getNextPageUrl(dom);
                    LazyLoad.scroll();
                });
            },
            
            getNextPageUrl: function(container) {
                /* Check if there are succeeding pages after the current one */
                var pagination = $(container).find(LazyLoad.pagination_Container);
                var currentPageUrl = pagination.find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).find('a').attr('href');
                LazyLoad.nextPageUrl = pagination.find(LazyLoad.pagination_Selector).find(LazyLoad.current_Selector).next().find('a').attr('href');
                /* If the next page is still the current page, it means we have reached the last page. */
                if (currentPageUrl==LazyLoad.nextPageUrl){
                    /* No more page to load. Blank out the next page URL. */
                    LazyLoad.nextPageUrl=null;
                }
                /* Remove the pagination buttons */
                pagination.remove();
            } ,
            
            back2top: function(action){
                var backtoTop = $('body').find('.hs-back2top');
                if (action=='init'){
                    backtoTop = $(LazyLoad.back2TopHtml),
                    backtoTop.insertBefore($('footer'));
                
                    /* Unbind/Bind the mouse click to our function */
                    $(function(){
                        $('.hs-back2top').off('click');
                        $('.hs-back2top').on('click', function(){
                            $('html, body').animate({
                                scrollTop: $("body").offset().top
                            }, 300);
                        });
                    });
                }
                if ( ($('div.ossn-page-loading-annimation').css('display')) == 'none'){
                    if ( ((action=='show') || (action=='hide')) && (backtoTop.hasClass('hs-back2top')) ){
                        if (action=='show'){
                            backtoTop.css('bottom','5px');
                        } else {
                            backtoTop.css('bottom','-100px');
                        }
                    }
                }
            },
            
            newsfeedScroll: function(action){
                if (action=='lock'){
                    if (!LazyLoad.newsfeed.lock){
                        LazyLoad.newsfeed.lock = true;
                        $(LazyLoad.newsfeed.container).css({
                            position:'fixed',
                            width:LazyLoad.newsfeed.width+'px',
                            bottom:LazyLoad.newsfeed.bottom+'px'
                        });
                    } else {
                        $(LazyLoad.newsfeed.container).css({
                            bottom:LazyLoad.newsfeed.bottom+'px'
                        });
                    }
                } else if (action=='release' && LazyLoad.newsfeed.lock){
                    LazyLoad.newsfeed.lock = false;
                    $(LazyLoad.newsfeed.container).css({position:'relative',bottom:'auto'});
                }
            },

            
            emojiis: function(items){
                var emojii = {
                    icon:$(LazyLoad.items_Container).find('.ossn-comment-attach-photo i.fa-smile-o').parent().first(),
                    dropdown:$(LazyLoad.items_Container).find('.dropdown.emojii-container-main').first()
                };
                if (!items.find('.ossn-comment-attach-photo i.fa-smile-o').length){
                    $(emojii.icon).clone().insertAfter(items.find('.ossn-comment-attach-photo'));
                    $(emojii.dropdown).clone().appendTo(items.find('.comment-container'));
                }
            },

        }
    
        var len = params.length;
        for (var i = 0; i < len; i++) {
            var args=params[i];
            if (($('body').find(args.items_Container).length) && ($('body').find(args.item_Entity).length)){
                LazyLoad.init(args);
                break;
            }
        }
    });
});
}
