<?php
/**
 *    OpenSource-SocialNetwork
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   General Public Licence http://opensource-socialnetwork.com/licence
 * @link      http://www.opensource-socialnetwork.com/licence
 */
if (!isset($params['user']->guid)) {
    $params['user'] = new stdClass;
    $params['user']->guid = '';
}
 ossn_load_external_js('places.min');
 ossn_load_external_js('jquery.tokeninput'); 
?>
<div style="background: #fff;">&nbsp;</div>
    <textarea placeholder="<?php echo ossn_print('wall:post:container'); ?>" name="post"></textarea>

    <div id="ossn-wall-photo" style="display:none;">
        <input type="file" name="ossn_photo"/>
    </div>

<div class="controls">
    <li class="ossn-wall-photo">
       <i class="fa fa-picture-o"></i>
    </li>
    <li class="top" title="" data-placement="right" data-toggle="tooltip" href="#" style="cursor: pointer"; data-original-title="Your profile information and personal details will be not visible to anyone." >
      <label><input class="post-anno" type="checkbox" name="anonymous" value="1">Post Anonymously</label>
    </li>  
    <div style="float:right;">
      
        <div class="ossn-loading ossn-hidden"></div> 
         <input class="btn btn-primary ossn-wall-post" type="submit" value="<?php echo ossn_print('post'); ?>"/>
    </div>
    <li class="ossn-cancel">
      <input type="reset" value="cancel" class="btn btn-primary ossn-wall-cancel" name="">
    </li>
</div>
<input type="hidden" value="<?php echo $params['user']->guid; ?>" name="wallowner"/>
<?php
  if(ossn_isAdminLoggedin()){
 ?>
<div class="row" style="margin-top: 10px;">
   <div class="col-md-6">
       <select class="form-control" name="post_type">
             <option value="UserPost">Discussion</option>
             <option value="walkin">Walkins</option>
       </select>
   </div>
   <div class="col-md-6">
       <select class="form-control" name="post_city">
             <option value="Bangalore">Bangalore</option>
             <option value="Hyderabad">Hyderabad</option>
             <option value="Chennai">Chennai</option>
             <option value="delhi"> Delhi/NCR</option>
             <option value="PUNE">PUNE</option>
             <option value="Kolkata">Kolkata</option>
             <option value="Mumbai">Mumbai</option>
       </select>
   </div>
</div>      
<?php
}
?>