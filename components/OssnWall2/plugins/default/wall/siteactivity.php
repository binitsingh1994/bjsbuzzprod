<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$wall       = new OssnWall;

$accesstype = ossn_get_homepage_wall_access();
$loggedinuser = ossn_loggedin_user();

// wall mode: all site posts
$posts = $wall->getPublicPosts(array( 
	'type' => 'user',
	'distinct' => false,
	'post_type'=>'UserPost',
	'approve'=> 1,
	'order_by'=>'update_time DESC'
));
$count = $wall->getPublicPosts(array(
	'type' => 'user',
	'count' => true,
	'distinct' => false,
	'approve'=> 1,
	'post_type'=>'UserPost'
));
if($posts) {
	foreach($posts as $post) {
		/* profile photo update remove from post */
		 if($post->item_type !='profile:photo'){
			 	$item = ossn_wallpost_to_item($post);
			 	echo ossn_wall_view_template($item);
		 }
	}
}
echo ossn_view_pagination($count);