<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
$wall       = new OssnWall;

$accesstype = ossn_get_homepage_wall_access();
$loggedinuser = ossn_loggedin_user();

// allow All to watch ALL postings independant of Wall setting
$posts = $wall->getAllPosts(array( 
	'type' => 'user',
	'distinct' => true,
	'post_type'=>'walkin',
	'order_by'=>'guid DESC',
)); 
$count = $wall->getAllPosts(array(
	'type' => 'user',
	'count' => true,
	'distinct' => true,
	'post_type'=>'walkin', 
));
if($posts) {
		foreach($posts as $post) {
				$item = ossn_wallpost_to_item($post);
				echo ossn_wall_view_template($item);
		}
}

echo ossn_view_pagination($count);
