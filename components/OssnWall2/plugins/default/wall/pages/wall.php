<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
if(!isset($params['user'])) {
		$params['user'] = '';
}
echo '<div class="ossn-wall-container">';
echo ossn_view_form('home/container', array(
		'action' => ossn_site_url() . 'action/wall/post/a',
		'component' => 'OssnWall',
		'id' => 'ossn-wall-form',
		'enctype' => 'multipart/form-data',
		'params' => array(
				'user' => $params['user']
		)
), false);
echo '</div>';
?> 
<div class="panel with-nav-tabs panel-default">
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#userpost" data-toggle="tab">Discussion</a></li>
            <li><a href="#walkin" data-toggle="tab">Walkins</a></li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="tab-content">
<?php
echo '<div class="user-activity tab-pane fade in active" id="userpost">';
echo ossn_plugin_view('wall/siteactivity');
echo '</div>';
echo '<div class="walkin-activity tab-pane fade in" id="walkin">';
echo ossn_plugin_view('wall/walkinactivity');
echo '</div>';
?>
		</div>
    </div>
</div>
