<?php
/**
 *    OpenSource-SocialNetwork
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   General Public Licence http://opensource-socialnetwork.com/licence
 * @link      http://www.opensource-socialnetwork.com/licence
 */

//declear empty variables;
$friends_c = '';
//init classes
$notification = new OssnNotifications;
$user_name = ossn_loggedin_user()->username;

 $search_type = ossn_loggedin_user()->search_type;
 $passout_year = ossn_loggedin_user()->passout_year;
 $qualification = ossn_loggedin_user()->qualification;
 $current_location = ossn_loggedin_user()->current_location;
 $domain = ossn_loggedin_user()->domain;
 $email = ossn_loggedin_user()->email;
 $gender = ossn_loggedin_user()->gender;
 $birthdate = ossn_loggedin_user()->birthdate;

if (ossn_loggedin_user()->first_name == 'firstname' || $email == '' || $email == NULL || $$birthdate = '' || $birthdate = NULL || ossn_loggedin_user()->last_name == 'lastname' || $search_type == NULL || $search_type == NULL || $passout_year == NULL || $qualification == NULL || $current_location == NULL || $domain == NULL) {
  
    $current_url = str_replace(":80","",current_url());
    $profile_edit_url = ossn_site_url("u/".$user_name."/edit");

    if (!($profile_edit_url == $current_url)) {
      ?>
      <script type="text/javascript"> location.href = "<?php echo $profile_edit_url; ?>"; </script>
    <?php
    }
  }

$count_notif = $notification->countNotification(ossn_loggedin_user()->guid);

if(class_exists('OssnMessages')){
  $messages = new OssnMessages;
  $count_messages = $messages->countUNREAD(ossn_loggedin_user()->guid);
}

$friends = ossn_loggedin_user()->getFriendRequests();
if (count($friends) > 0 && !empty($friends)) {
    $friends_c = count($friends);
}
?>
<li id="ossn-notif-friends">
    <a onClick="Ossn.NotificationFriendsShow(this);" class="ossn-notifications-friends" href="javascript:void(0);">
                       <span>
                      <?php if ($friends_c > 0) { ?>
                          <span class="ossn-notification-container"><?php echo $friends_c; ?></span>
                          <div class="ossn-icon ossn-icons-topbar-friends-new"><img class="img-responsive" src="<?php echo ossn_site_url(); ?>themes/social/images/add-contact.svg" width="24" height="24"></div>
                      <?php } else { ?>
                          <span class="ossn-notification-container hidden"></span>
                          <div class="ossn-icon ossn-icons-topbar-friends"><img class="img-responsive" src="<?php echo ossn_site_url(); ?>themes/social/images/add-contact.svg" width="24" height="24"></div>
                      <?php } ?>
                       </span>
    </a>
</li>
<?php if($messages){ ?>
<li id="ossn-notif-messages">
    <a onClick="Ossn.NotificationMessagesShow(this)" href="javascript:void(0);" class="ossn-notifications-messages" role="button" data-toggle="dropdown">
    
                       <span>
                        <?php if ($count_messages > 0) { ?>
                            <span class="ossn-notification-container"><?php echo $count_messages; ?></span>
                            <div class="ossn-icon ossn-icons-topbar-messages-new"><img class="img-responsive" src="<?php echo ossn_site_url(); ?>themes/social/images/comment.svg" width="24" height="24"></div>
                        <?php } else { ?>
                            <span class="ossn-notification-container hidden"></span>
                            <div class="ossn-icon ossn-icons-topbar-messages"><img class="img-responsive" src="<?php echo ossn_site_url(); ?>themes/social/images/comment.svg" width="24" height="24"></div>
                        <?php } ?>
                       </span>
    </a></li>
   <?php } ?> 
<li id="ossn-notif-notification">
    <a href="javascript:void(0);" onClick="Ossn.NotificationShow(this)" class="ossn-notifications-notification" onClick="Ossn.NotificationShow(this)"role="button" data-toggle="dropdown"> 
                       <span>
                       <?php if ($count_notif > 0) { ?>
                           <span class="ossn-notification-container"><?php echo $count_notif; ?></span>
                           <div class="ossn-icon ossn-icons-topbar-notifications-new"><img class="img-responsive" src="<?php echo ossn_site_url(); ?>themes/social/images/notification.svg "width="24" height="24"></div>
                       <?php } else { ?>
                           <span class="ossn-notification-container hidden"></span>
                           <div class="ossn-icon ossn-icons-topbar-notification"><img class="img-responsive" src="<?php echo ossn_site_url(); ?>themes/social/images/notification.svg" width="24" height="24"></div>
                       <?php } ?>
                       </span>
    </a>
 
</li>
  <div class="dropdown">
      <div class="dropdown-menu multi-level dropmenu-topbar-icons ossn-notifications-box">
               <div class="selected"></div>
               <div class="type-name"> <?php echo ossn_print('notifications'); ?> </div>
              <div class="metadata">
                  <div style="height: 66px;">
                        <div class="ossn-loading ossn-notification-box-loading"></div>
                  </div>
                  <div class="bottom-all">
                      <a href="#"><?php echo ossn_print('see:all'); ?></a>
                  </div>
             </div>
      </div> 
   </div>