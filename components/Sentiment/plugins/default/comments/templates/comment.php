<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */

ossn_trigger_callback('comment', 'load', $params['comment']);
$OssnLikes = new OssnLikes;
$comment = arrayObject($params['comment'], 'OssnWall');
$user = ossn_user_by_guid($comment->owner_guid);
if ($params['anonymous'] == '0' || $comment->owner_guid != $params['postOwner']) {
	$userPic = $user->iconURL()->smaller;
	$fullname = $user->fullname;
	$profileURL = $user->profileURL();
	$tag = "yes";
} else {
	$tag = "no";
	$userPic = '';
	$fullname = "Anonymous User";
	$profileURL = '';
	$userPic = ossn_theme_url() . 'images/nopictures/users/small.jpg';
	if (ossn_isAdminLoggedin() || $params['post']->owner_guid == ossn_loggedin_user()->guid) {
		$userPic = $user->iconURL()->smaller;
		$fullname = $user->fullname . ' ' . $anonymousTag;
		$profileURL = $user->profileURL();
	}
}

if ($comment->type == 'comments:post' || $comment->type == 'comments:entity') {
	$type = 'annotation';
}
$likes_total = $OssnLikes->CountLikes($comment->id, $type);
$datalikes = '';
if ($likes_total > 0) {
	$datalikes = $likes_total;
	$likes_total = "<i class='fa fa-thumbs-up'></i>" . $likes_total;
}
?>

</style>
<div class="comments-item" id="comments-item-<?php echo $comment->id; ?>">
    <div class="row">
        <div class="col-md-1">
            <img class="comment-user-img" src="<?php echo $userPic; ?>" />
        </div>
        <div class="col-md-11">
            <div class="comment-contents">
            <div class="ossn-comment-menu">
                <div class="dropdown">
                <?php
echo ossn_view_menu('comments', 'comments/menu/comments');
?>
                 </div>
            </div>
        <p>
            <?php
echo ossn_plugin_view('output/url', array(
	'href' => $profileURL,
	'text' => $fullname,
	'class' => 'owner-link',
));
if ($comment->type == 'comments:entity') {

	$message = $comment->getParam('comments:entity');
	$reg_exUrl = '#(www\.|https?://|http?://)?[a-z0-9]+\.[a-z0-9]{2,4}\S*#i';

	if (preg_match($reg_exUrl, $message, $url)) {
		echo '<span>' . preg_replace($reg_exUrl, '<a href="' . $url[0] . '" rel="nofollow">' . $url[0] . '</a>', $message) . '</span>';
	} else {
		echo '<span>' . $message . '</span>';
	}

} elseif ($comment->type == 'comments:post') {
	echo '<span>' . $comment->getParam('comments:post') . '</span>';
}
$image = $comment->getParam('file:comment:photo');
if (!empty($image)) {
	$image = str_replace('comment/photo/', '', $image);
	$image = ossn_site_url("comment/image/{$comment->id}/{$image}");
	echo "<img src='{$image}' />";
}
?>
        </p>
<div class="comment-metadata">
            <?php
if (isset($comment->sentiment) && !empty($comment->sentiment)) {
	echo ossn_plugin_view('sentiment/icon', array(
		'sentiment' => $comment->sentiment,
		'class' => 'time-created',
	));
}?>
			<div class="time-created"><?php echo ossn_user_friendly_time($comment->time_created); ?></div>
			<?php
if (ossn_isLoggedIn()) {
	if (!$OssnLikes->isLiked($comment->id, ossn_loggedin_user()->guid, $type)) {
		echo ossn_plugin_view('output/url', array(
			'href' => ossn_site_url("action/annotation/like?annotation={$comment->id}"),
			'text' => ossn_print('like'),
			'class' => 'ossn-like-comment',
			'data-type' => 'Like',
			'action' => true,
		));
	} else {
		echo ossn_plugin_view('output/url', array(
			'href' => ossn_site_url("action/annotation/unlike?annotation={$comment->id}"),
			'text' => ossn_print('unlike'),
			'class' => 'ossn-like-comment',
			'data-type' => 'Unlike',
			'action' => true,
		));
	}

} // Likes only for loggedin users end
// Show total likes
echo ossn_plugin_view('output/url', array(
	'href' => 'javascript:void(0);',
	'text' => $likes_total,
	'onclick' => "Ossn.ViewLikes({$comment->id}, 'annotation')",
	'class' => "ossn-total-likes ossn-total-likes-{$comment->id}",
	'data-likes' => $datalikes,
));
if ($params['anonymous'] == '0') {
	?>

                            <a id="reply-to-<?php echo $comment->id; ?>" data-show="<?php echo $comment->id; ?>">Reply
                            </a>
<?php
}
?>


        </div>
            </div>
        </div>
    </div>
</div>
<div class="reply-list-<?php echo $comment->id; ?>">
<?php
if (ossn_isLoggedIn()) {
	if (ossn_is_hook('comment', 'replieslist')) {
		echo ossn_call_hook('comment', 'replieslist', $params['comment']);
	}
	if (ossn_is_hook('comment', 'replies')) {
		echo ossn_call_hook('comment', 'replies', $params['comment']);
	}
}
?>
</div>