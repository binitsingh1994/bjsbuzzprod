<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */

$image = $params['image'];
if ($params['post']->anonymous == '0') {
	$userPic = $params['user']->iconURL()->small;
	$fullname = $params['user']->fullname;
	$profileURL = $params['user']->profileURL();
	$tag = "yes";
} else {
	$anonymousTag = " [ Anonymous Post] ";
	$tag = "no";
	$userPic = '';
	$fullname = "Anonymous User";
	$profileURL = '';
	$userPic = ossn_theme_url() . 'images/nopictures/users/small.jpg';
	if (ossn_isAdminLoggedin() || $params['post']->owner_guid == ossn_loggedin_user()->guid) {
		$userPic = $params['user']->iconURL()->small;
		$fullname = $params['user']->fullname . ' ' . $anonymousTag;
		$profileURL = $params['user']->profileURL();
	}
}
$current_url = current_url ($uport= '');
    if (strpos($current_url, 'home') !== false) {
        echo '112';
    } 
    else {
        echo '2';
    }
?>

<!-- wall item -->
<div class="ossn-wall-item" id="activity-item-<?php echo $params['post']->guid; ?>">
	<div class="row">
		<div class="meta">
			<img class="user-img" src="<?php echo $userPic; ?>" />
			<div class="post-menu">
				<div class="dropdown">
                 <?php
if (ossn_is_hook('wall', 'post:menu') && ossn_isLoggedIn()) {
	$menu['post'] = $params['post'];
	echo ossn_call_hook('wall', 'post:menu', $menu);
}
?>
				</div>
			</div>
			<div class="user">
           <?php if ($params['user']->guid == $params['post']->owner_guid && $params['post']->anonymous == 0) {?>
                <a class="owner-link"
                   href="<?php echo $params['user']->profileURL(); ?>"> <?php echo $params['user']->fullname; ?> </a>
            <?Php
} else {
	$owner = ossn_user_by_guid($params['post']->owner_guid);
	if ($params['post']->anonymous == 0) {
		?>
                <a href="<?php echo $profileURL; ?>">
                    <?php echo $fullname; ?>
                </a>
                <?php } else {

		echo '<span>' . $fullname . '</span>';
	}

	if ($tag == "yes") {?>
                <i class="fa fa-angle-right fa-lg"></i>
                <a href="<?php echo $profileURL; ?>"> <?php echo $fullname; ?></a>
                <?php }?>
            <?php }?>
			</div>
			<div class="post-meta">
				<a href="<?php echo ossn_site_url() . 'post/view/' . $params['post']->guid; ?>" target="_blank">
					<span class="time-created">
						<?php echo ossn_user_friendly_time($params['post']->time_created); ?>
					</span>
				</a>
                <span class="time-created"><?php echo $params['location']; ?></span>
                <?php
echo ossn_plugin_view('privacy/icon/view', array(
	'privacy' => $params['post']->access,
	'text' => '-',
	'class' => 'time-created',
));
if (isset($params['post']->sentiment) && !empty($params['post']->sentiment)) {
	echo ossn_plugin_view('sentiment/icon', array(
		'sentiment' => $params['post']->sentiment,
		'text' => '-',
		'class' => 'time-created',
	));
}
?>

			</div>
		</div>
		<div class="post-contents more">
			<p style="margin-bottom:0px"><?php echo stripslashes($params['text']); ?></p>
			 <?php
if (!empty($params['friends'])) {
	echo '<div class="friends">';
	foreach ($params['friends'] as $friend) {
		if (!empty($friend)) {
			$user = ossn_user_by_guid($friend);
			$url = $user->profileURL();
			$friends[] = "<a href='{$url}'>{$user->fullname}</a>";
		}
	}
	if (!empty($friends)) {
		echo implode(', ', $friends);
	}
	echo '</div>';
}
?>
            <p style="margin-bottom:0px"> <?php
if (!empty($image)) {
	?>
                <img src="<?php echo ossn_site_url("post/photo/{$params['post']->guid}/{$image}"); ?>"/>

            <?php }?>
			</p>
		</div>
		<div class="comments-likes">
			<ul class="menu-likes-comments-share">
				<?php echo ossn_view_menu('postextra', 'wall/menus/postextra'); ?>
			</ul>
         	<?php
if (ossn_is_hook('post', 'likes')) {
	echo ossn_call_hook('post', 'likes', $params['post']);
}
?>
			<div class="comments-list">
              <?php
if (ossn_is_hook('post', 'comments')) {
	echo ossn_call_hook('post', 'comments', $params['post']);
}
?>
			</div>
		</div>
	</div>
</div>
<!-- ./ wall item -->