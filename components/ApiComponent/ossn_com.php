<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

define('__API__', ossn_route()->com . 'ApiComponent/');
require_once __API__ . 'classes/ApiComponent.php';
require_once __API__ . 'libs/ApiComponent.php';
/**
 * Initialize Ossn Wall Component
 *
 * @return void
 * @access private
 */

function ApiComponent() {

	// ossn_register_action('users/login', __API__ . 'actions/user/login.php');
	ossn_register_page('apiUserLogin', 'api_component_userlogin');
	ossn_register_page('apiUserRegister', 'api_component_userregister');
	ossn_register_page('apiUserResetLogin', 'api_component_userresetlogin');
	ossn_register_page('apiUserResetPassword', 'api_component_userresetpassword');

}

function api_component_userlogin($pages) {
	$contents = array("content" => ossn_plugin_view("user/login"));
	$content = ossn_set_page_layout('empty', $contents);
	echo ossn_plugin_view("user/login");
}

function api_component_userregister($pages) {
	$contents = array("content" => ossn_plugin_view("user/register"));
	$content = ossn_set_page_layout('empty', $contents);
	echo ossn_plugin_view("user/register");
}

function api_component_userresetlogin($pages) {
	$contents = array("content" => ossn_plugin_view("user/resetlogin"));
	$content = ossn_set_page_layout('empty', $contents);
	echo ossn_plugin_view("user/resetlogin");
}

function api_component_userresetpassword($pages) {
	$contents = array("content" => ossn_plugin_view("user/resetpassword"));
	$content = ossn_set_page_layout('empty', $contents);
	echo ossn_plugin_view("user/resetpassword");
}
ossn_register_callback('ossn', 'init', 'ApiComponent');

