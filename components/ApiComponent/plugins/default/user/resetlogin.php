<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

// http://localhost/bjsbuzz_rewampp/apiUserResetLogin?email={{email}}


$user = input('email');
if (empty($user)) {
    $em['error'] = ossn_print('password:reset:email:required');
    echo json_encode($em);
}
$user = ossn_user_by_email($user);

if ($user && $user->SendResetLogin()) {
	$em['success'] = ossn_print('passord:reset:email:success');
    echo json_encode($em);
} else {
	$em['fail'] = ossn_print('passord:reset:fail');
    echo json_encode($em);
}