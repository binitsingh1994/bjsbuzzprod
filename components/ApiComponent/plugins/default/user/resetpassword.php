<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
*/

 // https://www.bjsbuzz.com/apiUserResetPassword?user=ashwin&c=a85cea6a11722c169697952956403736&password=ashwin123&re_password=ashwin1234

$user = input('user');
$code = input('c');
$password = input('password');
$re_password = input('re_password');

if ($password == $re_password) {
        if (empty($password)) {
            $em['error'] = ossn_print('password:error');
            echo json_encode($em);
        }
        if (!empty($user) && !empty($code)) {
            $user = ossn_user_by_username($user);
            if ($user && $code == $user->getParam('login:reset:code')) {
                if ($user->resetPassword($password)) {
                    $user->deleteResetCode();
                    $em['success'] = ossn_print('passord:reset:success');
                    echo json_encode($em);
                } else {
                    $em['error'] = ossn_print('passord:reset:fail');
                    echo json_encode($em);
                }
            } else {
                $em['error'] = ossn_print('passord:reset:fail');
                echo json_encode($em);
            }
        } else {
            $em['error'] = ossn_print('passord:reset:fail');
            echo json_encode($em);
        }
} else {
        $em['error'] = ossn_print('re_password:error');
        echo json_encode($em);
}