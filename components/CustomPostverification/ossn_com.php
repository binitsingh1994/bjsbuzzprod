<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
define('__CUSTOM_POSTVERIFICATION__', ossn_route()->com . 'CustomPostverification/');
require_once(__CUSTOM_POSTVERIFICATION__ . 'classes/CustomPostverification.php');
/**
 * Initialize Ossn Postverification Component
 *
 * @return void
 * @access private
 */
function Custom_postverification() {
	ossn_register_com_panel('CustomPostverification', 'settings');
	if(ossn_isAdminLoggedin()) {
		ossn_register_action('user/post/verification', __CUSTOM_POSTVERIFICATION__ . 'actions/user/post/verification.php');
		ossn_register_page('post_verification','ossn_post_verification_page');
	}
	ossn_add_hook('verification:template', 'verification', 'ossn_verification_templates');
	ossn_add_hook('notification:view','post:verification','ossn_verification_notify');
	ossn_extend_view('js/opensource.socialnetwork', 'js/verfication');
}
function ossn_post_verification_page(){
    $title = ossn_print('Post Verification');
    $contents = array("content" => ossn_plugin_view("postverification/pages/view"));
    $content = ossn_set_page_layout('newsfeed', $contents);
    echo ossn_view_page($title, $content);
}
function ossn_unvalidatepost_to_item($post) {
		if($post) {
				if(!isset($post->poster_guid)) {
						$post = ossn_get_object($post->guid);
				}
				$data     = json_decode(html_entity_decode($post->description));
				$text     = ossn_restore_new_lines($data->post, true);
				$location = '';
				
				if(isset($data->location)) {
						$location = '- ' . $data->location;
				}
				if(isset($post->{'file:wallphoto'})) {
						$image = str_replace('ossnwall/images/', '', $post->{'file:wallphoto'});
				} else {
						$image = '';
				}
				
				$user = ossn_user_by_guid($post->poster_guid);
				return array(
						'post' => $post,
						'friends' => explode(',', $data->friend),
						'text' => $text,
						'location' => $location,
						'user' => $user,
						'image' => $image
				);
		}
		return false;
}
function ossn_unvalidatepost_view_template(array $params = array()) {
		if(!is_array($params)){
			return false;
		}
		$type = 'verification';
		if(ossn_is_hook('verification:template', $type)) {
			return ossn_call_hook('verification:template', $type, $params);
		}
		return false;
}
function ossn_verification_templates($hook, $type, $return, $params) {
		ossn_trigger_callback('verification', 'load:item', $params);
		$params = ossn_call_hook('verification', 'templates:item', $params, $params);
		return ossn_plugin_view("postverification/templates/verification/item", $params);
}

function ossn_verification_notify($name, $type, $return, $params) {
    $notif = $params;

    $baseurl = ossn_site_url();
    $user = ossn_user_by_guid($notif->owner_guid);
    $user->fullname = "<strong>{$user->fullname}</strong>";

    $img = "<div class='notification-image'><img src='{$baseurl}avatar/{$user->username}/small' /></div>";

    $type = 'poke';
    $type = "<div class='ossn-notification-icon-poke'></div>";
    if ($notif->viewed !== NULL) {
        $viewed = '';
    } elseif ($notif->viewed == NULL) {
        $viewed = 'class="ossn-notification-unviewed"';
    }
    $url = $user->profileURL(); 
    $notification_read = "{$baseurl}post/view/{$notif->item_guid}";
    return "<a href='{$notification_read}'>
	       <li {$viewed}> {$img} 
		   <div class='notfi-meta'> {$type}
		   <div class='data'>".ossn_print("ossn:notifications:{$notif->type}", array($user->fullname)) . '</div>
		   </div></li>';

}

ossn_register_callback('ossn', 'init', 'Custom_postverification');
