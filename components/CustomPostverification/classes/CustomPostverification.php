<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
class CustomPostverification extends OssnObject {

		public function getUnvalidatedPosts($params = array()) {
				$user = ossn_loggedin_user();
				if(isset($user->guid) && !empty($user->guid)) {
						$friends      = $user->getFriends();
						//operator not supported for strings #999
						$friend_guids = array();
						if($friends) {
								foreach($friends as $friend) {
										$friend_guids[] = $friend->guid;
								}
						}
						// add all users posts;
						// (if user has 0 friends, show at least his own postings if wall access type = friends only)
						$friend_guids[] = $user->guid;
						$friend_guids   = implode(',', $friend_guids);
						
						$default = array(
								'type' => 'user',
								'subtype' => 'wall',
								'order_by' => 'update_time DESC',
								'entities_pairs' => array(
												array(
												  	'name' => 'access',
													'value' => true,
												  	'wheres' => "(1=1)"
												  ),
												array(
												  	'name' => 'poster_guid',
													'value' => true,
													'wheres' => "(emd0.value=2 OR emd0.value=3)"
												  )
								)
						);
						
						$options = array_merge($default, $params);
						return $this->searchUnvalidatedObject($options);
				}
				return false;
		}

		public function creatNotification($owner,$post){
			$data_config = new OssnDatabase;
			$type = 'post:verification';
			$poker = ossn_loggedin_user()->guid;
        	$params['into'] = 'ossn_notifications';
        	$params['names'] = array(
            	'type',
            	'poster_guid',
            	'owner_guid',
            	'subject_guid',
            	'item_guid',
            	'time_created'
        	);
			$params['values'] = array(
            	$type,
            	$poker,
            	$owner,
            	0,
            	$post,
            	time(),
            	

        	);
        	if($data_config->insert($params,true)){
        		return true;
        	}
        	return false;
		}
		
} //class