<?php

$data_config = new OssnDatabase;
$action = input('actions');
$post_id = input('post_id');

switch ($action) {
case 'approve':
	$name = array("update_time", "approve");
	$value = array(time(), "1");
	$params['table'] = 'ossn_object';
	$params['names'] = $name;
	$params['values'] = $value;
	$params['wheres'] = array(
		"guid='{$post_id}'",
	);
	if ($data_config->update($params)) {
		$notify = new CustomPostverification;
		$notify->creatNotification(ossn_get_object($post_id)->owner_guid, ossn_get_object($post_id)->guid);
		echo "1";
	}
	break;
case 'delete':
	$ossnwall = new OssnWall;
	$post = $ossnwall->GetPost($post_id);
	if ($ossnwall->deletePost($post_id)) {
		echo "1";
	}
	break;
default:
	break;
}
