<?php
if($params['post']->anonymous == "1"){
      $anonymous = "[ Anonymous ]";
  }
?>
<div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
               <section class="post-heading" style="margin-bottom: 18px;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="media">
                              <div class="media-left">
                                <a href="#">
                                  <img class="media-object photo-profile" src="<?php echo $params['user']->iconURL()->small; ?>" width="40" height="40" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <a  href="<?php echo $params['user']->profileURL(); ?>" class="anchor-username"><h4 class="media-heading"><?php echo $params["user"]->fullname.'   '.$anonymous; ?></h4></a>
                                <a href="#" class="anchor-time"><?php echo ossn_user_friendly_time($params['post']->time_created); ?>  <?php echo $params['location']; ?></a>
                              </div>
                            </div>
                        </div>
                         <div class="col-md-6">
                             <button type="button"  class="btn btn-success verification-action" post_id="<?php echo $params['post']->guid; ?>" data-action="approve">Approve</button>
                             <button type="button" class="btn btn-warning verification-action" post_id="<?php echo $params['post']->guid; ?>" data-action="delete">Delete</button>
                             
                             <a href="<?php echo ossn_site_url('action/block/user?user='.$params["user"]->guid, true);  ?>"  class="btn btn-danger verification-action" post_id="<?php echo $params['post']->guid; ?>" data-action="block">Block</a>
                         </div>
                    </div>            
               </section>
               <section class="post-body">
                   <p><?php 
                    if(stripcslashes($params['text'] == "null:data"))
                      echo "File Uploaded";
                    else
                        echo stripslashes($params['text']); 
                      ?>
                    </p>
             </section>
              
            </div>
        </div>  
  </div>