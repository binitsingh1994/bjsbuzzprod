<?php
$wall = new CustomPostverification;
$posts = $wall->getUnvalidatedPosts(array(
			'type' => 'user',
			'distinct' => true,
			'post_type'=>'UserPost',
));
if($posts) {
	foreach($posts as $post) {
		$item = ossn_unvalidatepost_to_item($post);
		echo ossn_unvalidatepost_view_template($item);
	}
}