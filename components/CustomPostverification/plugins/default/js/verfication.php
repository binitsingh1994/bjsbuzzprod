/**
 * 	Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence 
 * @link      https://www.opensource-socialnetwork.org/
 */
Ossn.RegisterStartupFunction(function() {
    $(document).ready(function() {
        $('.verification-action').click(function(){
            var elem  = $(this);
            var data = 'actions='+$(this).attr('data-action')+'&post_id='+$(this).attr('post_id');
            $url = "<?php echo ossn_site_url($extend = 'action/user/post/verification?'); ?>"+data;
            Ossn.PostRequest({
                url: $url,
                data:data,
                beforeSend: function(request) {
                    //$('#activity-item-' + $url.attr('data-guid')).attr('style', 'opacity:0.5;');
                },
                callback: function(callback) {
                    if(callback == 1){
                        elem.parent().closest('.col-md-12').remove();
                    }
                }
            });

        });
    });
});
