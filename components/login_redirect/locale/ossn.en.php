<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$en = array(
		'login_redirect' => 'Login Redirect',
		'login:redirect:url' => 'Redirection URL',
		'login:redirect:pattern' => 'Please enter URL below. Add [USERNAME] for loggedin user username. <br /> For example: http://mywebsite.com/u/[USERNAME]/',
		'login:redirect:saved:settings' => 'Setting has been saved',
		'login:redirect:saved:failed' => 'Settings save failed'
);
ossn_register_languages('en', $en);