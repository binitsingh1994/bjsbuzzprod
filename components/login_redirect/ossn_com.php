<?php
/**
 * Open Source Social Network
 *
 * @package   Ossn.Component
 * @author    SOFTLAB24 <info@softlab.24.com>
 * @copyright 2015 SOFTLAB24.COM
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.softlab24.com
 */
define('__LOGIN_REDIRECT__', ossn_route()->com . 'login_redirect/');

/**
 * Login redirect initialize
 *
 * @param string $callback Callback
 * @param string $type Callback type
 * @param array  $params options 
 *
 * @return void
 */
function login_redirect_init($callback, $type, $params) {
		ossn_register_com_panel('login_redirect', 'settings');
		if(ossn_isAdminLoggedin()) {
				ossn_register_action('login/redirect/url/save', __LOGIN_REDIRECT__ . 'actions/save.php');
		}
		ossn_register_callback('login', 'success', 'login_redirect');
}
/**
 * Login Redirect
 * Manage redirect
 *
 * @param string $callback login
 * @param string $type success
 * @param array $vars a user array object 
 *
 * @return void
 */
function login_redirect($callback, $type, $vars) {
		$user = $vars['user'];
		if($user) {
				$settings = new OssnComponents;
				$settings = $settings->getSettings('login_redirect');
				
				if(isset($settings->url) && !empty($settings->url)) {
					if($user->first_name == 'firstname' || $user->last_name == 'lastname'){
						$url = str_replace("[USERNAME]", $user->username, $settings->url);
						$url = str_replace("[ID]", $user->guid, $url);
						$url = str_replace(ossn_site_url(), '', $url);
						
						redirect($url);
					}else{
						redirect();
					}
				}
		}
}
ossn_register_callback('ossn', 'init', 'login_redirect_init');
