<?php
/**
 * Open Source Social Network
 *
 * @package   Ossn.Component
 * @author    SOFTLAB24 <info@softlab.24.com>
 * @copyright 2015 SOFTLAB24.COM
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.softlab24.com
 */
$settings = new OssnComponents;
$settings = $settings->getSettings('login_redirect');

$label         = ossn_print('login:redirect:url');
$label_pattern = ossn_print('login:redirect:pattern');
$input         = ossn_plugin_view("input/text", array(
		'name' => 'url',
		'value' => $settings->url
));

$form = <<<FORM
<div>
	<label>$label</label>
	<p>$label_pattern</p>
	$input
</div>
<div>
	<input type='submit' class='btn btn-primary'/>
</div>
FORM;
echo $form;