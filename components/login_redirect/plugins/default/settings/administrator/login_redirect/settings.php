<?php
/**
 * Open Source Social Network
 *
 * @package   Ossn.Component
 * @author    SOFTLAB24 <info@softlab.24.com>
 * @copyright 2015 SOFTLAB24.COM
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.softlab24.com
 */
 
echo ossn_view_form('login_redirect/save', array(
    'action' => ossn_site_url() . 'action/login/redirect/url/save',
    'class' => 'ossn-admin-form'	
));