<?php
/**
 * Open Source Social Network
 *
 * @package   Ossn.Component
 * @author    SOFTLAB24 <info@softlab.24.com>
 * @copyright 2015 SOFTLAB24.COM
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.softlab24.com
 */
$url       = input('url');
$component = new OssnComponents;

if(empty($url)){
	ossn_trigger_message(ossn_print('login:redirect:saved:failed'), 'error');
	redirect(REF);
}
$vars = array(
		'url' => $url
);
if($component->setSettings('login_redirect', $vars)) {
		ossn_trigger_message(ossn_print('login:redirect:saved:settings'));
} else {
		ossn_trigger_message(ossn_print('login:redirect:saved:failed'), 'error');
}

redirect(REF);