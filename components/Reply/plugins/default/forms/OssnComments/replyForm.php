<?php
	$object = $params['object'];
?>
<span type="text" name="reply" id="reply-box-<?php echo $object; ?>" class="comment-box"
       placeholder="<?php echo ossn_print('write:reply'); ?>" contenteditable="true"></span>
<input type="hidden" name="post" value="<?php echo $object; ?>"/>
