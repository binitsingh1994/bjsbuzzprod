Ossn.Reply = function($container){
	$('#reply-box-' + $container).keypress(function(e) {
        if (e.which == 13) {
            if (e.shiftKey === false) {
            	//Postings and comments with same behaviour #924
                $replace_tags = function(input, allowed) {
                    allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('')
                    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi
                    var commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi
                    return input.replace(commentsAndPhpTags, '').replace(tags, function($0, $1) {
                        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : ''
                    })
                };
                $text = $('#reply-box-' + $container).html();
                $text = $replace_tags($text, '<br>').replace(/\<br\\?>/g, "\n");
                $('#reply-container-' + $container).append("<textarea name='comment' class='hidden'>" + $text + "</textarea>");
                $('#reply-container-' + $container).submit();
            }
        }
    });
    $('#reply-box-' + $container).on('paste', function(e) {
        e.preventDefault();
        var text = (e.originalEvent || e).clipboardData.getData('text/plain') || prompt('Paste something..');
        window.document.execCommand('insertText', false, text);
    });
    Ossn.ajaxRequest({
        url: Ossn.site_url + 'action/post/reply',
        form: '#reply-container-' + $container,
        beforeSend: function(request) {
            $('#reply-box-' + $container).attr('readonly', 'readonly');
            $('#reply-box-' + $container).attr('contenteditable', false);
        },
        callback: function(callback) {
        	if (callback['process'] == 1) {
                $('#reply-box-' + $container).removeAttr('readonly');
                $('#reply-box-' + $container).val('');
                $(callback['comment']).insertBefore('.reply-list-' + $container+' .reply-item');
                $('#reply-box-' + $container).attr('readonly', 'false');
                $('#reply-box-' + $container).attr('contenteditable', true);
                $('#reply-box-' + $container).html('');
            }
       	}    
    });
}
Ossn.RegisterStartupFunction(function() {
    $(document).ready(function() {
        $("[id^=reply-to-]").click(function(){
            $('.reply-list-'+$(this).attr('data-show')+' .reply-item').show();
            $("#reply-box-" +$(this).attr('data-show')).focus();
        }); 

        $(document).delegate('.ossn-delete-reply', 'click', function(e) {
            e.preventDefault();
            $reply = $(this);
            $url = $reply.attr('href');
            $reply_id = Ossn.UrlParams('reply', $url);
            if(confirm("Are you sure you want to delete!") == false){
                return false;
            }
            Ossn.PostRequest({
                url: $url,
                action: false,
                beforeSend: function() {
                    $('.reply-item-' + $reply_id).attr('style', 'opacity:0.6;');
                },
                callback: function(callback) {
                    console.log(callback);
                    if (callback == 1) {
                        $('.reply-item-' + $reply_id).fadeOut().remove();
                    }
                    if (callback == 0) {
                        $('.reply-item-' + $reply_id).attr('style', 'opacity:0.6;');
                    }
                }
            });
        });

    });
});

