
<?php
$option = (object)$params;

if($option->reply_guid != ""){
	$user = ossn_user_by_guid($option->replier_guid);
?>
	<div class="reply-item-<?php echo $option->reply_guid; ?>" id="reply-block">
		<div class="commenterImage">
			<img src="<?php echo $user->iconURL()->smaller; ?>" style="margin-right: 10px; border-radius: 32px;"/>
			<div class="commenterName">
			<a href="<?php echo $user->profileURL(); ?>"><?php echo $user->first_name." ".$user->last_name; ?></a>
		</div>

		</div>
		
      		<?php 
			if((ossn_loggedin_user()->guid == $user->guid) || ossn_isAdminLoggedin()){
			?>
				<div class="btn-group pull-right">
      				<div type="button" class="pull-right btn btn-white dropdown-toggle" data-toggle="dropdown">
    					<span class="caret"></span>
      				</div>
					<ul class="dropdown-menu" style="margin: 0px 0px;" role="menu">
	        			<li>
	          				<a class="ossn-delete-reply" href="<?php echo ossn_site_url().'action/post/delete?reply='.$option->reply_guid; ?>" style="color: red;">Delete</a>
	        			</li>
	     			</ul>
	     		</div>
    		<?php
    			}
    		?>
    	
		<div class="commentText">
			<?php $message = $option->reply_text; ?>
			<?php
			$reg_exUrl ='#(www\.|https?://|http?://)?[a-z0-9]+\.[a-z0-9]{2,4}\S*#i';

			if(preg_match($reg_exUrl, $message, $url)) {
			       echo "<span>".preg_replace($reg_exUrl, '<a target="_blank" href="'.$url[0].'" rel="nofollow">'.$url[0].'</a>', $message)."</span>";
			} else {
			       echo "<span>".$message."</span>";
			}
			?>
	    	<div class="reply-metadata"> 
	    	<span class="time-created"><?php echo ossn_user_friendly_time($option->time_created); ?></span>
	    	<a class="ossn-like-comment" data-type="Like"></a>                    
		  	<a id="reply-to-<?php echo $option->comment_id; ?>" data-show="<?php echo $option->comment_id; ?>">Reply</a>
		  </div>
		</div>
</div>
		 
	        	
                
<?php
	}
?>
