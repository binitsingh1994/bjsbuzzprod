<?php
$reply = new Reply;
$id = $params['id'];
$data = $reply->getCommentReply($id);
foreach ($data as $value) {
    if(!empty($value)){
        echo $data = ossn_reply_view($value);    
    }
}
?>
<?php
$user = ossn_loggedin_user();
	$iconurl = $user->iconURL()->smaller;
    $inputs = ossn_view_form('replyForm', array(
        'action' => ossn_site_url() . 'action/post/reply',
        'component' => 'OssnComments',
        'id' => "reply-container-{$params['id']}",
        'class' => 'reply-container',
        'autocomplete' => 'off',
        'params' => array('object' => $params['id'])
    ), false);

$form = <<<html
<div class="reply-item" style="display:none">
    <div class="row">
        <div class="col-md-1">
            <img class="comment-user-img" src="{$iconurl}" />
        </div>
        <div class="col-md-11">
            $inputs
        </div>
    </div>
</div>
html;
$form .= '<script>  Ossn.Reply('.$params['id'].'); </script>';
echo $form;