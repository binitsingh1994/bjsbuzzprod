<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
define('__REPLY__', ossn_route()->com . 'Reply/');
require_once(__REPLY__ . 'classes/Reply.php');
require_once(__REPLY__ . 'libs/reply.php');
/**
 * Initialize Ossn Wall Component
 *
 * @return void 
 * @access private
 */ 
function reply() {
	ossn_add_hook('comment', 'replies', 'reply_to_comment');
	ossn_add_hook('comment', 'replieslist', 'comment_reply_list');
	ossn_extend_view('js/opensource.socialnetwork', 'js/Reply');
	if(ossn_isLoggedIn()){
		ossn_register_callback('reply', 'create', 'comment_reply_add', 1);
		ossn_register_action('post/reply', __REPLY__ . 'actions/post/reply.php');
		ossn_register_action('post/delete', __REPLY__ . 'actions/post/delete.php');
	}
	ossn_add_hook('notification:view','comment:replyNotification','ossn_reply_notify');
}

function comment_reply_add($callback, $type, $params) {
		if(isset($params['id']) && !empty($params['id'])) {
				$comment = ossn_get_annotation($params['id']);
		} elseif($params['annotation']) {
				$comment = ossn_get_annotation($params['annotation']->id);
		}
		if($comment) {
				if($comment->type == 'comments:entity') {
						$text = $comment->getParam('comments:entity');
				} elseif($comment->type == 'comments:post') {
						$text = $comment->getParam('comments:post');
				}
				if(!empty($text)) {
						$sentiment = sentiment_process($text);
						if(!empty($sentiment)) {
								$comment->data->sentiment = $sentiment;
								$comment->save();
						}
				}
		}
}

function reply_to_comment($hook, $type, $return, $params) {
	ossn_trigger_callback('replies', 'load:item', $params);
	$params = ossn_call_hook('replies', 'templates:item', $params, $params);
	return ossn_plugin_view("reply/templates/reply/item", $params);
}

function comment_reply_list($hook,$type,$return,$params){
	ossn_trigger_callback('replies', 'load:item', $params);
	$params = ossn_call_hook('replies', 'templates:item', $params, $params);
	return ossn_plugin_view("reply/templates/reply", $params);
}

function ossn_reply_view($params, $template = 'reply'){
	$vars = ossn_call_hook('reply:view', 'template:params', $params, $params);
	return ossn_plugin_view("reply/templates/{$template}", $vars);
}

function ossn_reply_notify($name, $type, $return, $params) {
    $notif = $params;

    $baseurl = ossn_site_url();
    $user = ossn_user_by_guid($notif->owner_guid);
    $poster = ossn_user_by_guid($notif->poster_guid);
    $user->fullname = "<strong>{$user->fullname}</strong>";

    $img = "<div class='notification-image'><img src='{$baseurl}avatar/{$poster->username}/small' /></div>";

    $type = 'poke';
    $type = "<div class='ossn-notification-icon-poke'></div>";
    if ($notif->viewed !== NULL) {
        $viewed = '';
    } elseif ($notif->viewed == NULL) {
        $viewed = 'class="ossn-notification-unviewed"';
    }
    $url = $user->profileURL(); 
    $notification_read = "{$baseurl}post/view/{$notif->item_guid}";
    return "<a href='{$notification_read}'>
	       <li {$viewed}> {$img} 
		   <div class='notfi-meta'> {$type}
		   <div class='data'>".$poster->fullname.' '.ossn_print("ossn:notifications:{$notif->type}", array($user->fullname)) . '</div>
		   </div></li>';

}
ossn_register_callback('ossn', 'init', 'reply');