<?php
$reply = new Reply;
$reply_text = input('comment');
$comment = input('post');
if($reply->PostReply($comment,$reply_text,ossn_loggedin_user()->guid)){
	$vars  = array();
	$db = new OssnDatabase;
	$params['from'] = "Comment_reply";
	$params['wheres'] = array("reply_guid ={$reply->getReplyId()}");
	$get = $db->select($params, true);
	foreach ($get as $item) {
		$vars = (array)$item;
	}
	$data = ossn_reply_view($vars);
	
	if(!ossn_is_xhr()) {
		redirect(REF);
	} else {
				$reply->creatReplyNotification($comment);
				header('Content-Type: application/json');
				echo json_encode(array(
						'comment' => $data,
						'process' => 1
				));
		}
} else {
		if(!ossn_is_xhr()) {
				redirect(REF);
		} else {
				header('Content-Type: application/json');
				echo json_encode(array(
						'process' => 0
				));
		}		
}
