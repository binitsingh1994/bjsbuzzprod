<?php
class Reply extends OssnAnnotation {

	public function PostReply($subject_id , $reply_text ,$replier ){
		if(empty($subject_id) || empty($reply_text)) {
			return false;
		}
		$canreply = false;
		if(!empty($reply_text)) {
			$canreply = true;
		}

		$this->reply_text = $reply_text;
		$this->subject_id = $subject_id;
		$this->replier = $replier;
		if($canreply && $this->addReply()){
			return true;
		}
	}

	public function getReplyId() {
		return $this->getInsertedReplyId();
	}

	public function getCommentReply($id){
		$db = new OssnDatabase;
		$params['from'] = "Comment_reply";
		$params['wheres'] = array("comment_id ='".$id."'");
		$get = $db->select($params, true);
		foreach ($get as $reply) {
			$vars[] = (array)$reply;
		}
		return $vars;
	}

	public function creatReplyNotification($commentId){
			$data_config = new OssnDatabase;

			$comment = new OssnComments;
			$commentObj = $comment->GetComment($commentId);
			$post = $commentObj->subject_guid;
			$owner = $commentObj->owner_guid;
			$type = 'comment:replyNotification';
			$poker = ossn_loggedin_user()->guid;

        	$params['into'] = 'ossn_notifications';
        	$params['names'] = array(
            	'type',
            	'poster_guid',
            	'owner_guid',
            	'subject_guid',
            	'item_guid',
            	'time_created'
        	);
			$params['values'] = array(
            	$type,
            	$poker,
            	$owner,
            	$commentId,
            	$post,
            	time(),
        	);
        	if($poker == $owner){
        		return true;
        	}
        	else{
        		if($data_config->insert($params,true)){
        			return true;
        		}	
        	}
        	
        	return false;
	}

	public function DeleteReply($replyId){
		$data_config = new OssnDatabase;
		$params['from'] = 'Comment_reply';
		$params['wheres'] = array("reply_guid={$replyId}");
		if($data_config->delete($params,true)){
			return true;
		}
		else {
			return false;
		}
	}

	public function GetInstantReply(){
		$db = new OssnDatabase;
		$params['from'] = 'Comment_reply';
		$time_lap = Time() - 5;
		$params['wheres'] = array("time_created < '".Time()."' AND  time_created > '".$time_lap."'");
		$replys = $db->select($params, true);
		foreach ($replys as $reply ) {
			$var = (array)$reply;
			$vars  = array();
			$data['subject_data'] = ossn_reply_view($reply);
			$data['subject_guid'] = $var['comment_id'];
			$data['reply_id'] = $var['reply_guid'];
			$response[] = $data;
		}
		return $response;
	} 
}