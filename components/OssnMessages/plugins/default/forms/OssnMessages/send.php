                <textarea id="message" name="message" placeholder="<?php echo ossn_print('message:placeholder'); ?>"></textarea>
                <input type="hidden" name="to" value="<?php echo $params['user']->guid; ?>"/>

                <div class="controls">
                    <?php 
                    //this form should be in OssnMessages/forms 
                    echo ossn_plugin_view('input/security_token'); 
                    ?>
                    <div class="ossn-loading ossn-hidden"></div>                               
                    <input type="submit" class="btn btn-primary sendButton" value="<?php echo ossn_print('send'); ?>"/>
                </div>
                <script type="text/javascript">
                   $(document).ready(function(){
                        $('.sendButton').attr('disabled',true);
                        $('#message').keyup(function(){
                            if($(this).val().length !=0)
                                $('.sendButton').attr('disabled', false);            
                            else
                                $('.sendButton').attr('disabled',true);
                        })
                    });

                </script>