<?php
/**
 * Open Source Social Network
 *
 * @package   (softlab24.com).ossn
 * @author    OSSN Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

function ossn_system_info() {
   ossn_register_page('ossninfo', 'ossn_system_info_page');
}

function ossn_system_info_page(){
	echo "<style>
	table {
    border-collapse: collapse;
	}
	td, th {
    border: 1px solid #000000;
    font-size: 100%;
    vertical-align: baseline;
	padding: 5px;
	}
	body, td, th, h1, h2 {
    font-family: sans-serif;
	}
	</style>";

	echo "<h2>Ossn System Info</h2>";
	echo "<h3>PHP Settings</h3>";
	echo "<table>";
	echo "<tr><td>Version</td><td>" . PHP_VERSION . "</td></tr>";
	echo "<tr><td>Server APi</td><td>" . PHP_SAPI . "</td></tr>";
	echo "<tr><td>display_errors</td><td>" . ini_get('display_errors') . "</td></tr>";
	echo "<tr><td>&nbsp;</td><td></td></tr>";
	echo "<tr><td>memory_limit</td><td>" . ini_get('memory_limit') . "</td></tr>";
	echo "<tr><td>register_globals</td><td>" . ini_get('register_globals') . "</td></tr>";
	echo "<tr><td>post_max_size</td><td>" . ini_get('post_max_size') . "</td></tr>";
	echo "<tr><td>upload_max_filesize</td><td>" . ini_get('upload_max_filesize') . "</td></tr>";
	echo "<tr><td>default_charset</td><td>" . ini_get('default_charset') . "</td></tr>";
	echo "</table>";

	echo "<h3>Ossn Environment</h3>";
	echo "<table>";
	$users = new OssnUser;
	$total_members = $users->countByGender('female') + $users->countByGender('male');
	echo "<tr><td>Version</td><td>" . ossn_site_settings('site_version') . "</td></tr>";
	echo "<tr><td>Site URL</td><td>" . ossn_site_url() . "</td></tr>";
	echo "<tr><td>Operating System</td><td>" . php_uname() . "</td></tr>";
	$install_dir = ossn_route()->www;
	echo "<tr><td>Install Dir</td><td>" . $install_dir . "</td></tr>";
	if (function_exists('posix_getpwuid')) {
		$owner_perms = posix_getpwuid(fileowner($install_dir))['name'] . " [" . substr(sprintf('%o', fileperms($install_dir)), -4);
	}
	else {
		$owner_perms = 'n/a';
	}
	echo "<tr><td>Install Dir Owner [Perms]</td><td>" . $owner_perms . "]</td></tr>";
	$data_dir = ossn_get_userdata();
	echo "<tr><td>Data Dir</td><td>" . $data_dir . "</td></tr>";
	if (function_exists('posix_getpwuid')) {
		$owner_perms = posix_getpwuid(fileowner($data_dir))['name'] . " [" . substr(sprintf('%o', fileperms($data_dir)), -4);
	}
	else {
		$owner_perms = 'n/a';
	}
	echo "<tr><td>Data Dir Owner [Perms]</td><td>" . $owner_perms . "]</td></tr>";
	echo "<tr><td>Site Notification Email</td><td>" . ossn_site_settings('notification_email') . "</td></tr>";
	echo "<tr><td>Cache State</td><td>" . ossn_site_settings('cache') . "</td></tr>";
	echo "<tr><td>Members</td><td>" . $total_members . "</td></tr>";
	echo "<tr><td>Error Reporting</td><td>" . ossn_site_settings('display_errors') . "</td></tr>";
	echo "</table>";

	$checksum_file = $install_dir . 'components/system-info/checksums.md5';
	if (file_exists($checksum_file)) {
		unlink($checksum_file);
	}

	echo "<h3>Ossn Components</h3>";
	echo "<table>";
	echo "<tr><td>Component</td><td>Active</td><td>Checksum</td></tr>";
	$coms = new OssnComponents;
	$all_coms = $coms->getComponents();
	foreach($all_coms as $com) {
		$com_details = $coms->getbyName($com);
		$dir = $install_dir . 'components/' . $com_details->com_id;
		$checksum = MD5_DIR($dir, $checksum_file);
		echo "<tr><td>" . $com_details->com_id . "</td><td>" . $com_details->active . "</td><td>" . $checksum . "</td></tr>";
		unlink($checksum_file);
	}
	echo "</table>";

	echo "<h3>Ossn Themes</h3>";
	echo "<table>";
	echo "<tr><td>Theme</td><td>Active</td><td>Checksum</td></tr>";
	$themes = new OssnThemes;
	$all_themes = $themes->getThemes();
	$active_theme = $themes->getActive();
	foreach($all_themes as $theme) {
		$dir = $install_dir . 'themes/' . $theme;
		$checksum = MD5_DIR($dir, $checksum_file);
		$active = preg_match('/' . $theme . '/', $active_theme);
		echo "<tr><td>" . $theme . "</td><td>" . $active . "</td><td>" . $checksum . "</td></tr>";
		unlink($checksum_file);
	}
	echo "</table>";

	echo "<h3>Ossn Core</h3>";
	echo "<table>";
	echo "<tr><td>Division</td><td>Checksum</td></tr>";
	$devisions = array('actions', 'classes', 'libraries', 'locale', 'system', 'upgrade', 'vendors');
	foreach($devisions as $devision) {
		$dir = $install_dir . '/' . $devision;
		$checksum = MD5_DIR($dir, $checksum_file);
		echo "<tr><td>" . $devision . "</td><td>" . $checksum . "</td></tr>";
		unlink($checksum_file);
	}
	echo "</table>";

}

function MD5_DIR($dir, $checksum_file) {
	$file = '';
    if (!is_dir($dir)) {
        return false;
    }
    $d = dir($dir);
    while (false !== ($entry = $d->read())) {
        if ($entry != '.' && $entry != '..') {
             if (is_dir($dir.'/'.$entry)) {
				 MD5_DIR($dir.'/'.$entry, $checksum_file);
             }
             else {
				 $md5_hash = md5_file($dir.'/'.$entry) . "\n";
				 file_put_contents($checksum_file, $md5_hash, FILE_APPEND);
             }
        }
    }
    $d->close();
	$checksums = explode("\n", file_get_contents($checksum_file));
	array_pop($checksums);
	sort($checksums);
	$sorted_checksums = implode("\n", $checksums);
	file_put_contents($checksum_file, $sorted_checksums);
	file_put_contents($checksum_file, "\n", FILE_APPEND);
	return md5_file($checksum_file);
}

ossn_register_callback('ossn', 'init', 'ossn_system_info');
