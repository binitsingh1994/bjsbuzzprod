<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */

$walkin =  explode('/', getenv('REQUEST_URI'));
if(in_array("walkin", $walkin)){
	$activeWalkin = "active";
}else{
	$activehome = "active";
}

if(!isset($params['user'])) {
		$params['user'] = '';
}
echo '<div class="ossn-wall-container">';
echo ossn_view_form('home/container', array(
		'action' => ossn_site_url() . 'action/wall/post/a',
		'component' => 'OssnWall',
		'id' => 'ossn-wall-form',
		'enctype' => 'multipart/form-data',
		'params' => array(
				'user' => $params['user']
		)
), false);
echo '</div>';
?>
<div>
 <ul class="nav nav-tabs" role="tablist">
   <li role="presentation" class="<?php echo $activehome; ?>"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Discussion</a></li>
   <li role="presentation" class="<?php echo $activeWalkin; ?>" ><a href="#walkin" aria-controls="walkin" role="tab" data-toggle="tab">Walkins</a></li>
   <!-- <li role="presentation"><a href="#fresherjob" aria-controls="fresherjob" role="tab" data-toggle="tab">Fresher Jobs</a></li>
   <li role="presentation"><a href="#expjobs" aria-controls="expjobs" role="tab" data-toggle="tab">Experience Jobs</a></li> -->
 </ul>

 <!-- Tab panes -->
 <div class="tab-content">
   <div role="tabpanel" class="tab-pane <?php echo $activehome; ?>" id="home"><?php echo ossn_plugin_view('wall/siteactivity'); ?></div>
   <div role="tabpanel" class="tab-pane <?php echo $activeWalkin; ?>" id="walkin"><?php echo ossn_plugin_view('wall/walkinactivity'); ?></div>
   <!-- <div role="tabpanel" class="tab-pane" id="fresherjob"><?php //echo ossn_plugin_view('wall/fresherjobactivity'); ?></div>
   <div role="tabpanel" class="tab-pane" id="expjobs"><?php //echo ossn_plugin_view('wall/expjobactivity'); ?></div> -->
 </div>

</div>
<?php


