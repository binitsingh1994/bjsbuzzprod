<?php /** * Open Source Social Network * * @package Open Source Social Network * @author Open Social Website Core Team <info@softlab24.com> * @copyright 2014-2017 SOFTLAB24 LIMITED * @license Open Source Social Network License (OSSN LICENSE) http://www.opensource-socialnetwork.org/licence * @link https://www.opensource-socialnetwork.org/ */ ?>
<style type="text/css">
  
  .canvas {
    -webkit-box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
    -moz-box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
    box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
    width: 400px;
    height: 400px;
    margin: auto;
    background-color: #eee;
    position: relative;
    border: 2px solid #DDD;
    box-sizing: border-box;
  }
  
  .canvas > div {
    width: 100px;
    height: 100px;
    position: absolute;
    line-height: 100px;
    font-size: 2.4em;
    background-color: #F1F1F1;
    color: #4B5F6D;
    font-weight: bold;
    font-family: "Segoe UI Light", sans-serif;
    box-sizing: border-box;
    border: 1px solid #DDD;
  }
  
  .canvas > div:not(.null) {
    transition: .1s ease left, .1s ease top;
    z-index: 50;
  }
  
  .win {
    position: fixed;
    transform: translate(-50%, -50%);
    left: 50%;
    top: 50%;
    z-index: 600;
    border-radius: 15px;
    background-color: #fff;
    box-shadow: 0 0 10px 900px rgba(0, 0, 0, .3), 0 0 15px rgba(0, 0, 0, 0.5);
    text-transform: uppercase;
    padding: 30px;
    display: none;
  }
  
  .win p {
    font-size: 4em;
  }
  
  .btn {
    display: inline-block;
    border-radius: 3px;
    background-color: #fff;
    border: 1px solid #000;
    padding: 5px 15px;
    font-weight: bold;
  }
  
  a {
    color: #4384F5;
    text-decoration: none;
  }
  
  a:hover {
    opacity: 0.8;
  }
  
  svg {
    vertical-align: middle;
  }
  /* ==========================================================================
   Main Styles
   ========================================================================== */
  
  .content {
    display: table-cell;
    font-size: 22px;
    text-align: center;
    vertical-align: middle;
    padding: 40px 30px;
  }
  
  .inner-wrapper {
    display: inline-block;
  }
  
  .top-title {
    color: #4384F5;
    font-size: 35px;
    font-weight: 700;
    margin-bottom: 25px;
  }
  
  .main-title {
    line-height: 0;
    font-size: 90px;
    font-weight: 800;
    color: #4384F5;
  }
  
  .svg-wrap {
    display: inline-block;
    font-size: 0;
    vertical-align: super;
  }
  
  #lego {
    padding: 5px;
  }
  
  .blurb {
    margin-top: 30px;
    color: #B6BECC;
  }
  
  .lego-btn {
    background: #4384F5;
    border-radius: 4px;
    color: #fff;
    display: inline-block;
    margin-top: 30px;
    padding: 12px 25px;
  }
  /* ==========================================================================
   Media Queries
   ========================================================================== */
  
  @media only screen and (max-width: 440px) {
    .main-title {
      font-size: 60px;
    }
    #lego {
      width: 50px;
      height: 55px;
      padding: 10px 5px;
    }
    .blurb,
    .lego-btn {
      font-size: 18px;
    }
  }
  @media only screen and (max-width: 440px) {
    .canvas {
      -webkit-box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
    -moz-box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
    box-shadow: 0 0 30px rgba(0, 0, 0, 0.3);
    width: 400px;
    height: 400px;
    margin: auto;
    background-color: #eee;
    position: relative;
    border: 2px solid #DDD;
    box-sizing: border-box;
    }
    #lego {
      width: 50px;
      height: 55px;
      padding: 10px 5px;
    }
    .blurb,
    .lego-btn {
      font-size: 18px;
    }
  }
</style>
<div class="row">
  <div class="col-md-11 ossn-page-contents">
    <div class="ossn-error-page">
      <div class="col-md-6">
        <div class="content">

          <!-- top container -->
          <div class="inner-wrapper">
            <div class="top-title">Oh!</div>
            <div class="main-title">
              4
              <span class="svg-wrap">
          <svg width="80px" height="80px" viewBox="0 0 40 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
            <title>Lego head</title>
            <defs>
              <path id="path-1" d="M14.00976,6.04673974 C15.1748273,5.5908863 16,4.45702525 16,3.13043478 C16,1.40154339 14.5984566,0 12.8695652,0 C12.0080647,0 11.2278445,0.348002376 10.6618113,0.911100458 C10.5772973,0.939953572 10.4903214,0.971146886 10.4008152,1.00475543 C5.57957133,2.8150777 4.24944679,0.347826087 3.13043478,0.347826087 C1.40154339,0.347826087 0,1.74936948 0,3.47826087 C0,5.20715226 1.40154339,6.60869565 3.13043478,6.60869565 C4.46967336,6.60869565 12.3802688,6.97312235 14.00976,6.04673974 L14.00976,6.04673974 Z"></path>
            </defs>
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
              <g id="Lego-head" sketch:type="MSLayerGroup" transform="translate(1.000000, 1.000000)">
                <g id="Lego">
                  <g id="404-Page">
                    <g id="Group">
                      <g id="Lego-head">
                        <rect id="head-bottom" stroke="#D8B140" stroke-width="2" fill="#F4CD3B" sketch:type="MSShapeGroup" x="9" y="29" width="20" height="11" rx="2"></rect>
                        <rect id="head-top" stroke="#D8B140" stroke-width="2" fill="#F4CD3B" sketch:type="MSShapeGroup" x="11" y="0" width="16" height="11" rx="2"></rect>
                        <rect id="head" stroke="#D8B140" stroke-width="2" fill="#F4CD3B" sketch:type="MSShapeGroup" x="0" y="4" width="38" height="32" rx="8"></rect>
                        <g id="mouth" transform="translate(11.000000, 23.000000)">
                          <g id="mask-2" fill="#FFFFFF" sketch:type="MSShapeGroup">
                            <path d="M14.00976,6.04673974 C15.1748273,5.5908863 16,4.45702525 16,3.13043478 C16,1.40154339 14.5984566,0 12.8695652,0 C12.0080647,0 11.2278445,0.348002376 10.6618113,0.911100458 C10.5772973,0.939953572 10.4903214,0.971146886 10.4008152,1.00475543 C5.57957133,2.8150777 4.24944679,0.347826087 3.13043478,0.347826087 C1.40154339,0.347826087 0,1.74936948 0,3.47826087 C0,5.20715226 1.40154339,6.60869565 3.13043478,6.60869565 C4.46967336,6.60869565 12.3802688,6.97312235 14.00976,6.04673974 L14.00976,6.04673974 Z" id="path-1"></path>
                          </g>
                          <g id="Mask" fill="#3D3D3D" sketch:type="MSShapeGroup">
                            <path d="M14.00976,6.04673974 C15.1748273,5.5908863 16,4.45702525 16,3.13043478 C16,1.40154339 14.5984566,0 12.8695652,0 C12.0080647,0 11.2278445,0.348002376 10.6618113,0.911100458 C10.5772973,0.939953572 10.4903214,0.971146886 10.4008152,1.00475543 C5.57957133,2.8150777 4.24944679,0.347826087 3.13043478,0.347826087 C1.40154339,0.347826087 0,1.74936948 0,3.47826087 C0,5.20715226 1.40154339,6.60869565 3.13043478,6.60869565 C4.46967336,6.60869565 12.3802688,6.97312235 14.00976,6.04673974 L14.00976,6.04673974 Z" id="path-1"></path>
                          </g>
                          <g id="Oval-25-Clipped">
                            <mask id="mask-2" sketch:name="path-1" fill="white">
                              <use xlink:href="#path-1"></use>
                            </mask>
                            <g id="path-1"></g>
                            <path d="M2.90917969,7.67480469 C5.90917969,9.95019531 8.99755822,7.576474 8.99755822,6.74804688 C8.99755822,5.91961975 6.209139,4 4,4 C1.790861,4 2.8444044e-16,4.67157288 2.8444044e-16,5.5 C2.8444044e-16,6.32842712 -0.0908203125,5.39941406 2.90917969,7.67480469 L2.90917969,7.67480469 Z" id="Oval-25" fill="#EA6767" sketch:type="MSShapeGroup" mask="url(#mask-2)"></path>
                          </g>
                        </g>
                        <circle id="e-r" fill="#3D3D3D" sketch:type="MSShapeGroup" cx="13.5" cy="18.5" r="2.5"></circle>
                        <circle id="e-l" fill="#3D3D3D" sketch:type="MSShapeGroup" cx="22.5" cy="18.5" r="2.5"></circle>
                        <path d="M9,15.7883986 C9,15.7883986 11.0517578,11.8870314 13.6416016,13.3137892" id="e-l" stroke="#3D3D3D" sketch:type="MSShapeGroup" transform="translate(11.320801, 14.394199) scale(-1, -1) translate(-11.320801, -14.394199) "></path>
                        <path d="M26.6416016,15.7883986 C26.6416016,15.7883986 24.5898438,11.8870314 22,13.3137892" id="e-r" stroke="#3D3D3D" sketch:type="MSShapeGroup" transform="translate(24.320801, 14.394199) scale(-1, -1) translate(-24.320801, -14.394199) "></path>
                      </g>
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </svg>
        </span> 4
            </div>
          </div>

          <!-- bottom-tagline -->
          <p class="blurb">
            The page you were looking for doesn't exist.
            <br/> Return to
            <a href="http://bjslearnings.in/">homepage</a> or play game
          </p>

          <a class="btn lego-btn hidden-xs" href="#">New Game</a>

        </div>


      </div>
      <div class="col-md-6 hidden-xs">
        <p>Moves: <span id="moves">0</span> - Use arrow keys to move pieces and arrange numbers from 1 to 15</p>

        <div class="canvas"></div>

        <div class="win">
          <p>You win !</p>
          <a class="btn" href="#">New Game</a>
        </div>

        <p>

        </p>


      </div>

    </div>
  </div>
</div>
<script type="text/javascript">
  /**
   * Puzzle Class
   *
   * @constructor
   */
  var Puzzle = function Puzzle(el) {
    this.matrix = [];
    this.el = document.querySelector(el);
    this.movesEl = document.getElementById('moves');
    this._moves = 0;
    this._lock = false;
  };

  Puzzle.getNeighbors = function(index) {
    var result = {
      top: null,
      bottom: null,
      left: null,
      right: null
    };

    if ([3, 7, 11, 15].indexOf(index) === -1) {
      result.right = index + 1;
    }

    if ([0, 4, 8, 12].indexOf(index) === -1) {
      result.left = index - 1;
    }

    if (index > 3) {
      result.top = index - 4;
    }

    if (index < 12) {
      result.bottom = index + 4;
    }

    return result;
  };

  Puzzle.getPositionByIndex = function(index) {
    return [Math.floor(index % 4) * 100, Math.floor(index / 4) * 100];
  };

  Puzzle.prototype.getNeighbors = function(index) {
    var neighbors = Puzzle.getNeighbors(index);

    for (var position in neighbors) {
      var value = neighbors[position];
      if (value !== null) {
        neighbors[position] = this.matrix[value];
      }
    }

    return neighbors;
  };

  Puzzle.prototype.shuffle = function() {
    var matrix = this.matrix;
    var tempArray = [];
    var n = matrix.length;
    for (var i = 0; i < n; i++) {
      tempArray.push(matrix.splice(Math.floor(Math.random() * matrix.length), 1)[0]);
    }

    this.matrix = tempArray;

    return this;
  };

  Puzzle.prototype.createNumbers = function() {
    for (var i = 0; i < 16; i++) {
      this.matrix[i] = new Piece(this);
      this.matrix[i].el = document.createElement('div');
      this.matrix[i].el.innerHTML = i + 1;
      this.matrix[i].realIndex = i;

      this.el.appendChild(this.matrix[i].el);
    }

    this.matrix[15].el.innerHTML = '';
    this.matrix[15].el.className = 'null';
    this.matrix[15].isNull = true;

    return this;
  };

  Puzzle.prototype.updatePositions = function() {
    for (var i = 0; i < this.matrix.length; i++) {
      var obj = this.matrix[i];
      var position = Puzzle.getPositionByIndex(i);
      obj.el.style.left = position[0] + 'px';
      obj.el.style.top = position[1] + 'px';
    }

    return this;
  };

  Puzzle.prototype.__defineGetter__('nullPiece', function() {
    for (var i = 0; i < this.matrix.length; i++) {
      var obj = this.matrix[i];
      if (obj.isNull) {
        return obj;
      }
    }
  });

  Puzzle.prototype.move = function(direction) {
    if (this._lock) {
      return;
    }

    var nullPiece = this.nullPiece,
      neighbors = nullPiece.getNeighbors(),
      toReplace;

    switch (direction) {
      case 'up':
        toReplace = neighbors.bottom;
        break;
      case 'down':
        toReplace = neighbors.top;
        break;
      case 'left':
        toReplace = neighbors.right;
        break;
      case 'right':
        toReplace = neighbors.left;
        break;
    }

    if (toReplace !== null) {
      nullPiece.replace(toReplace);
      this.moves++;
      this.updatePositions();
      this.checkWin();
    }
  };

  Puzzle.prototype.checkWin = function() {
    for (var i = 0; i < this.matrix.length; i++) {
      var obj = this.matrix[i];
      if (obj.realIndex !== obj.index) {
        return false;
      }
    }

    document.querySelector('.win').style.display = 'block';
    this.lock();

    return true;
  };

  Puzzle.prototype.__defineGetter__('moves', function() {
    return this._moves;
  });

  Puzzle.prototype.__defineSetter__('moves', function(moves) {
    this._moves = moves;
    this.movesEl.innerHTML = this._moves;
  });

  Puzzle.prototype.unlock = function() {
    this._lock = false;
  };

  Puzzle.prototype.lock = function() {
    this._lock = true;
  };

  /**
   * Pieces Class
   *
   * @param puzzle
   * @constructor
   */
  var Piece = function Piece(puzzle) {
    this.el = null;
    this.parent = puzzle;
    this.isNull = false;
    this.realIndex = 0;
  };

  Piece.prototype.__defineGetter__('index', function() {
    return this.parent.matrix.indexOf(this);
  });

  Piece.prototype.getNeighbors = function() {
    return this.parent.getNeighbors(this.index);
  };

  Piece.prototype.replace = function(piece) {
    if (piece) {
      var myIndex = this.index,
        thatIndex = piece.index;

      this.parent.matrix[myIndex] = piece;
      this.parent.matrix[thatIndex] = this;

      return this;
    }
  };

  var puzzle = new Puzzle('.canvas').createNumbers().shuffle().updatePositions();

  window.addEventListener('keydown', function(e) {
    var key = e.keyCode;

    switch (key) {
      case 37:
        puzzle.move('left');
        e.preventDefault();
        break;
      case 38:
        puzzle.move('up');
        e.preventDefault();
        break;
      case 39:
        puzzle.move('right');
        e.preventDefault();
        break;
      case 40:
        puzzle.move('down');
        e.preventDefault();
        break;
    }
  });

  function newGame() {
    puzzle.moves = 0;
    puzzle.shuffle().updatePositions().unlock();
  }

  var buttons = document.querySelectorAll('.btn');

  for (var i = 0; i < buttons.length; i++) {
    buttons[i]
      .addEventListener('click', function(e) {
        e.preventDefault();

        newGame();
      });
  }

  document.querySelector('.win').addEventListener('click', function(e) {
    e.preventDefault();

    document.querySelector('.win').style.display = 'none';
  });
</script>