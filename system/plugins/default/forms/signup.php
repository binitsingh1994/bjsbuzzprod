<?php
/**
 * Open Source Social Network
 *
 * @package Open Source Social Network
 * @author    Open Social Website Core Team <info@softlab24.com>
 * @copyright 2014-2017 SOFTLAB24 LIMITED
 * @license   Open Source Social Network License (OSSN LICENSE)  http://www.opensource-socialnetwork.org/licence
 * @link      https://www.opensource-socialnetwork.org/
 */
?>
<style type="text/css">
    .display-none {
        display: none !important;
    }
</style>
<div>
    <input type="text" class = 'display-none' name="firstname" value = 'firstname' required placeholder="<?php echo ossn_print('first:name'); ?>"/>
    <input type="text" name="lastname" class = 'display-none' value = 'lastname' required placeholder="<?php echo ossn_print('last:name'); ?>"/>
</div>

<div>
    <input type="text" name="email" required placeholder="<?php echo ossn_print('email'); ?>"/>
    <input name="email_re" type="text" required placeholder="<?php echo ossn_print('email:again'); ?>"/>
</div>

<div>
    <input type="text" name="username" required placeholder="<?php echo ossn_print('username'); ?>" class="long-input"/>
</div>

<div>
    <input type="password" name="password" minlength="6" required placeholder="<?php echo ossn_print('password'); ?>" class="long-input"/>
</div>
<?php
function ossn_default_user_fieldss() {
        $fields             = array();
        $fields['required'] = array(
                'text' => array(
                        array(
                                'name' => 'birthdate',
                                'params' => array(
                                        'readonly' => true,
                                        'value' => date("d/m/Y"),
                                        'class' => 'display-none'
                                )
                        )
                ),
                'radio' => array(
                        array(
                                'name' => 'gender',
                                'options' => array(
                                        'male' => ossn_print('male'),
                                        'female' => ossn_print('female')
                                )
                        )
                )
        );
        return ossn_call_hook('user', 'default:fields', false, $fields);
}
$fields = ossn_default_user_fieldss();
if($fields){
			$vars	= array();
			$vars['items'] = $fields;
			echo ossn_plugin_view('user/fields/item', $vars);
}
?>
<div>
<?php echo ossn_fetch_extend_views('forms/signup/before/submit'); ?>
</div>

<div id="ossn-signup-errors" class="alert alert-danger"></div>

<p>
    <?php echo ossn_print('account:create:notice'); ?>
    <?php //Loosing typed in data when clicking Terms and Conditions link #620 ?>
    <a target="_blank" href="<?php echo ossn_site_url('site/terms'); ?>"><?php echo ossn_print('site:terms'); ?></a>
</p>
<div class="ossn-loading ossn-hidden"></div>
<input type="submit" id="ossn-submit-button" class="btn btn-success" value="<?php echo ossn_print('create:account'); ?>" class=""/>
